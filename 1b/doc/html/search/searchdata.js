var indexSectionsWithContent =
{
  0: "acdghmps",
  1: "cs",
  2: "cgmps",
  3: "achps",
  4: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Variables",
  4: "Macros"
};

