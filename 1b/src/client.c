/**
 * @file client.c
 * @author Alexander Halbarth <alexander.halbarth@student.tuwien.ac.at>
 * @date 2017-11-04
 *
 * @brief Client for OSUE exercise 1B `Battleship'.
 * @details This is the battleship client application
 * It automatically shoots at the ships from the server
 * The game map is sized 10x10 fields. There are 2 ships  with length 2, 3 with length 3 and one with length 4. Default port is 1280.
 * The game is limited by 80 rounds, after that the server wins, if the client did not shoot all ships. See common.h for more Default values!
 *
 * Communication is established over a network socket.
 * 
 * usage: ./client [-p PORT] [-h hostname]
 */
 
 // IO, C standard library, POSIX API, data types:
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <getopt.h>

// Assertions, errors, signals:
#include <assert.h>
#include <errno.h>
#include <signal.h>

// Time:
#include <time.h>

// Sockets, TCP, ... :
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>

// stuff shared by client and server:
#include "common.h"
#include "colors.h"


static char* argv0; /**< The program name */

static char* port=DEFAULT_PORT; /**< The port to use */
static char* host=DEFAULT_HOST; /**< The host to use */

// Static variables for resources that should be freed before exiting:
static struct addrinfo *ai = NULL;      /**< addrinfo struct */
static int sockfd = -1;                 /**< socket file descriptor*/

static void prt_usage(void); /**< Print usage Prototype */
static void send_position(unsigned int,unsigned int); /**< send position to server Prototype */
static void get_next_position(int*,int*); /**< get next position from algorithm Prototype */
static void cleanup(void); /**< cleanup Prototype*/

static uint8_t ship_map[MAP_SIZE][MAP_SIZE]; /**< Array to store game field*/

/**
 * Program entry point.
 * @brief The program starts here. This function takes care about parameters, 
 * Starts the socket and and opens a connection. When the connection is established it sends the coordinates to shoot at to the server 
 * supplied by the get_next_postion() method via the send_position() method.
 * After each shot it listens for a response to the server to know what was on the field.
 * When the game is finished it performes a clean-up and terminates with EXIT_SUCCESS. 
 * If there are errors it terminates with EXIT_FAILURE prints an error message to stderr and prints out the usage on usage failures.
 * 
 * @details global variables: 
 * argv0 Program name
 * addresinfo Address Information struct for socket
 * sockfd File descriptor of the socket
 * @param argc The argument counter.
 * @param argv The argument vector.
 * @return Returns EXIT_SUCCESS on Sucess and EXIT_FAILURE on Failure.
 */
int main(int argc, char* argv[]) {
	//Handling options
	argv0=argv[0];
	int c;
	while((c=getopt(argc,argv,"p:h:"))!=-1) {
		switch (c) {
			case 'h': //Handling Hostname
				if(optarg!=NULL) {
					host=optarg;
				} else {
					fprintf(stderr, "[%s] ERROR: No Host supplied\n",argv0);
					prt_usage();
					exit(EXIT_FAILURE);
				}
				break;
			case 'p': //Handling Port
				if(optarg!=NULL) {
					port=optarg;
				} else {
					fprintf(stderr, "[%s] ERROR: No Port supplied\n",argv0);
					prt_usage();
					exit(EXIT_FAILURE);
				}
				break;
			default: //error on wrong argument!
				prt_usage();
				exit(EXIT_FAILURE);
				break;
		}
	}
	
	if((argc-optind) != 0) { //no more arguments supplied
		prt_usage();
        exit(EXIT_FAILURE);
	}

	struct addrinfo hints;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	int res = getaddrinfo(host, port, &hints, &ai); //set socket address in ai
	if(res!=0) {
		fprintf(stderr,"[%s] ERROR: getaddrinfo %s\n",argv0,gai_strerror(res));
		exit(EXIT_FAILURE);
	}

	sockfd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol); //create socket
	if(sockfd<0) {
		cleanup();
		fprintf(stderr,"[%s] ERROR: socket file descriptor: %s\n",argv0,strerror(errno));
		exit(EXIT_FAILURE);
	}

	res = connect(sockfd, ai->ai_addr, ai->ai_addrlen); //connect to server
	if(res!=0) {
		cleanup();
		fprintf(stderr,"[%s] ERROR: connect %s\n",argv0,strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	int x = -1; //X coordinate to shoot at
	int y = -1; //Y coordinate to shoot at

	for(int round = 0; round<MAX_ROUNDS; round++) { //do until rounds are up
		print_map(ship_map);
		get_next_position(&x,&y); //gets next position to shoot at from Algorithm
		send_position(x,y); //send position to server
		uint8_t answer;
		res=recv(sockfd,&answer,sizeof(answer),MSG_WAITALL); //recieve response from server
		if(res>0) { //recv was successful
			switch((answer&12)/4) { //Check Status Bits
				case STATUS_GAME_OVER: //Status = GAME_OVER
					cleanup();
       	    		//char* winner = (((answer & 3)==HIT_LAST_SHIP_SUNK) ? "Client" : "Server");
        	        //fprintf(stdout,"[%s] %sINFO: GAME OVER. %s won!%s\n",argv0,KGRN,winner,KNRM);
					if((answer & 3)==HIT_LAST_SHIP_SUNK) {
						fprintf(stdout,"[%s] %sI win! :)%s\n",argv0,KGRN,KNRM);
					} else {
						fprintf(stdout,"[%s] %sI lost! :(%s\n",argv0,KGRN,KNRM);
					}
					exit(EXIT_SUCCESS);
               		break;
				case STATUS_PARITY_ERROR: //Status = PARITY Error
					fprintf(stderr,"[%s] %sERROR: parity error!%s\n",argv0,KRED,KNRM);
					cleanup();
					exit(EXIT_FAILURE);
					break;
				case STATUS_COORD_ERROR: //Status = Coordinate Error
					fprintf(stderr,"[%s] %sERROR: coordinate error!%s\n",argv0,KRED,KNRM);
					cleanup();
					exit(EXIT_FAILURE);
					break;
				case 0: //Status= Game running
					switch(answer&3) { //Check Hit Bits
						case HIT_NOTHING:
							debug_print("%s\n","RESPONSE: HIT_NOTHING!");
							ship_map[x][y]=SQUARE_EMPTY;
							break;
						case HIT_SHIP:
							debug_print("RESPONSE: %sHIT_SHIP!%s\n",KCYN,KNRM);
							ship_map[x][y]=SQUARE_HIT;
							break;
						case HIT_SHIP_SUNK:
							debug_print("RESPONSE: %sHIT_SHIP_SUNK!%s\n",KMAG,KNRM);
							ship_map[x][y]=SQUARE_HIT; //Mark all fields around the ship as EMPTY:
							if((x>0 && ship_map[x-1][y]==SQUARE_HIT)||(x<MAP_SIZE-1 && ship_map[x+1][y]==SQUARE_HIT)) {//ship horizontal
								int i=0;
								while(x-i<MAP_SIZE && ship_map[x-i+1][y]==SQUARE_HIT) { //Get to the most right position
									i--;
								}
								int xt = x-i;
								for(i=0;xt-i>=0 && ship_map[xt-i][y]==SQUARE_HIT;i++) { //mark up/down
									if(y+1<MAP_SIZE && ship_map[xt-i][y+1]==SQUARE_UNKNOWN) ship_map[xt-i][y+1]=SQUARE_EMPTY;
									if(y>0          && ship_map[xt-i][y-1]==SQUARE_UNKNOWN) ship_map[xt-i][y-1]=SQUARE_EMPTY;
								}
								if(xt-i>=0) { //mark front
									if(y+1<MAP_SIZE && ship_map[xt-i][y+1]==SQUARE_UNKNOWN) ship_map[xt-i][y+1]=SQUARE_EMPTY;
									if(y>0          && ship_map[xt-i][y-1]==SQUARE_UNKNOWN) ship_map[xt-i][y-1]=SQUARE_EMPTY;
									if(ship_map[xt-i][y]==SQUARE_UNKNOWN) ship_map[xt-i][y]=SQUARE_EMPTY;
								}
								if(xt+1<MAP_SIZE) { //mark back
									if(y+1<MAP_SIZE && ship_map[xt+1][y+1]==SQUARE_UNKNOWN) ship_map[xt+1][y+1]=SQUARE_EMPTY;
									if(y>0          && ship_map[xt+1][y-1]==SQUARE_UNKNOWN) ship_map[xt+1][y-1]=SQUARE_EMPTY;
									if(ship_map[xt+1][y]==SQUARE_UNKNOWN) ship_map[xt+1][y]=SQUARE_EMPTY;
								}
							} else if ((y>0 && ship_map[x][y-1]==SQUARE_HIT)||(y<MAP_SIZE-1 && ship_map[x][y+1]==SQUARE_HIT)) {//ship vertical
								int i=0;
								while(y-i<MAP_SIZE && ship_map[x][y-i+1]==SQUARE_HIT) { //Get to the most down position
									i--;
								}
								int yt = y-i;
								for(i=0; yt-i>=0 && ship_map[x][yt-i]==SQUARE_HIT;i++) { //mark left/right
									if(x+1<MAP_SIZE && ship_map[x+1][yt-i]==SQUARE_UNKNOWN) ship_map[x+1][yt-i]=SQUARE_EMPTY;
									if(x>0          && ship_map[x-1][yt-i]==SQUARE_UNKNOWN) ship_map[x-1][yt-i]=SQUARE_EMPTY;
								}
								if(yt-i>=0) { //mark front
									if(x+1<MAP_SIZE && ship_map[x+1][yt-i]==SQUARE_UNKNOWN) ship_map[x+1][yt-i]=SQUARE_EMPTY;
									if(x>0          && ship_map[x-1][yt-i]==SQUARE_UNKNOWN) ship_map[x-1][yt-i]=SQUARE_EMPTY;
									if(ship_map[x][yt-i]==SQUARE_UNKNOWN) ship_map[x][yt-i]=SQUARE_EMPTY;
								}
								if(yt+1<MAP_SIZE) { //mark back
									if(x+1<MAP_SIZE && ship_map[x+1][yt+1]==SQUARE_UNKNOWN) ship_map[x+1][yt+1]=SQUARE_EMPTY;
									if(x>0          && ship_map[x-1][yt+1]==SQUARE_UNKNOWN) ship_map[x-1][yt+1]=SQUARE_EMPTY;
									if(ship_map[x][yt+1]==SQUARE_UNKNOWN) ship_map[x][yt+1]=SQUARE_EMPTY;
								}
							}
							break;
						default:
							fprintf(stderr,"[%s] %sERROR: Hit code or Status code wrong!%s\n",argv0,KRED,KNRM);
							cleanup();
							exit(EXIT_FAILURE);
							break;
					}
					break;
				default: //Error undefined Status recieved!
					fprintf(stderr,"[%s] %sERROR: STATUS code wrong!%s\n",argv0,KRED,KNRM);
					cleanup();
					exit(EXIT_FAILURE);
					break;
			}
        } else if (res==0) { //end of transmission!
			cleanup();
			exit(EXIT_SUCCESS);
		} else { //error on recv!
			cleanup();
			fprintf(stderr,"[%s] ERROR: recv %s\n",argv0,strerror(errno));
			exit(EXIT_FAILURE);
		}
	}// Server did not exit!
	fprintf(stderr,"[%s] ERROR: MAX_ROUNDS reached!\n",argv0);
	cleanup();
	return EXIT_FAILURE;
}

/**
 *  @brief prints the usage of the program to stderr
 *  
 *  @details usage: ./client [-h HOSTNAME] [-p PORT]
 *   */
static void prt_usage() {
	fprintf(stderr, "usage: %s [-h HOSTNAME] [-p PORT]\n", argv0);
}

/**
 *  @brief Sends the coordinates to shoot at to the server
 *  
 *  @param [in] x X coordinate 0 to 9 (= A to J)
 *  @param [in] y Y coordinate 0 to 9
 *  
 *  @details Encodes the coordinates, adds the parity bit and sends the shot to the server. Terminates on wrong coordinates!
 */
static void send_position(unsigned int x, unsigned int y) {
	if(x>=MAP_SIZE||y>=MAP_SIZE) {
		fprintf(stderr, "[%s] %sERROR: invalid coordinate %c%i%s\n",argv0, KRED,'A'+x,y,KNRM);
		cleanup();
		exit(EXIT_FAILURE);
	}
	uint8_t message = x+y*10; //calculate message
	int c = 0;
	for(uint8_t i = 1; i<=64;i*=2 ) {	//count all bits which are 1
		if((message & i) > 0) {
			c++;
		}
	}
	if(c%2 == 1) { //check if parity is uneven
		message = message | 128; //set parity bit on uneven parity
	}
	if(send(sockfd,&message,sizeof(message),0)<sizeof(message)) { //send coordinates and check if successful
		fprintf(stderr,"[%s] ERROR: send %s\n",argv0,strerror(errno));
		exit(EXIT_FAILURE);
	} else {
		debug_print("coordinate %c%i sent\n",'A'+x,y);
	}
}

/**
 *  @brief Implementation of a checkerboard algorithm which shoots in an checkerbord pattern to find ships and the eliminates the ship
 *  
 *  @param [out] *x pointer to write x coordinate to
 *  @param [out] *y pointer to write y coordinate to
 *  @return in *x and *y the coordinates to shoot at next
 *  
 *  @details uses ship_map to calculate next shot!
 */
static void get_next_position(int *x, int *y) {
	static int last_x = -1; // last x shot to
	static int last_y = -1; // last y shot to
	if(last_x == -1 || last_y == -1) { // first call?
		*x=0;
		*y=0;
		last_x = *x;
        last_y = *y;
	} else {
		static int mode = 0; // mode checkerboard=0, search=1, horiz=2&4, vert=3&5
		static int exit_point_x; // x coordinate of point to shoot next in the checkerboard
		static int exit_point_y; // y coordinate of point to shoot next in the checkerboard
		static int search_point_x; // x coordinate of point where ship was hit first for search mode
		static int search_point_y; // y coordinate of point where ship was hit first for search mode
		
		do { //do until the next shot goes to an unknown field
			if(ship_map[last_x][last_y]==SQUARE_UNKNOWN) { //error handling to check if ship_map works
				fprintf(stderr,"[%s] ERROR: Misuse of shipmap!\n",argv0);
				cleanup();
				exit(EXIT_FAILURE);
			}
			switch(mode) { //different modes of algorithm: 0=checkerboard, 1=search ship orientation, 2=shoot horizontal left, 4=shoot horizontal right, 3=shoot vertical up, 4=shoot vertical down
				default: //checkerboard search
					;static int inv = 0; //inversion flag for failsafe
					if(ship_map[last_x][last_y]!=SQUARE_HIT) { //last shot was not a hit
						if((last_x%2)!=((last_y+inv)%2)) { //continue checkerboard algorithm. inv flag inverts the checkerboard!
								*x = last_x+1;
						} else {
							*x = last_x +2;
						}
						*y = last_y;
						if(*x>=MAP_SIZE) { //go to next row
								*x = (last_y+1)%2;
								*y = last_y+1;
						}
						if(*y>=MAP_SIZE) { //FAILSAFE! on the end of the field
							*x=1;
							*y=0;
							inv=1;
						}
					} else { //ship was hit, change mode, set search- & exit_point and run through the loop again!
						mode=1;
						*x = last_x;
						*y = last_y;
						search_point_x = last_x;//first hit to ship
						search_point_y = last_y;

						if((last_x%2)!=((last_y+inv)%2)) {
							exit_point_x = last_x+1;//where to continue checkerboard algorithm
						} else {
							exit_point_x = last_x +2;
						}
						exit_point_y = last_y;
						if(exit_point_x>=MAP_SIZE) { //go to next row
								exit_point_x = (last_y+1)%2;
								exit_point_y = last_y+1;
						}
						if(exit_point_y>=MAP_SIZE) { //FAILSAFE! on the end of the field
								exit_point_x=1;
								exit_point_y=0;
							inv=1;
						}
					}
					break;
				case 1: //search orientation of ship mode
					if(search_point_x>0){ //search left the hit point
						switch(ship_map[search_point_x-1][search_point_y]) {
							case SQUARE_HIT:
								mode=2;
								*x = search_point_x;
								*y = search_point_y;
								break;
							case SQUARE_UNKNOWN:
								*x = search_point_x-1;
								*y = search_point_y;
								break;
							default:
								break;
						}
					}
					if(mode==1 && search_point_y>0) { //search above of the hit point
						switch(ship_map[search_point_x][search_point_y-1]) {
							case SQUARE_HIT:
								mode=3;
								*x = search_point_x;
								*y = search_point_y;
								break;
							case SQUARE_UNKNOWN:
								*x = search_point_x;
								*y = search_point_y-1;
								break;
							default:
								break;
						}
					}
					if(mode==1 && search_point_x<MAP_SIZE-1){ //search right of the hit point
						switch(ship_map[search_point_x+1][search_point_y]) {
							case SQUARE_HIT:
								mode=2;
								*x = search_point_x;
								*y = search_point_y;
								break;
							case SQUARE_UNKNOWN:
								*x = search_point_x+1;
								*y = search_point_y;
								break;
							default:
								break;
						}
					}
					if(mode==1 && search_point_y<MAP_SIZE-1) { //search below the hit point
						switch(ship_map[search_point_x][search_point_y+1]) {
							case SQUARE_HIT:
								mode=3;
								*x = search_point_x;
								*y = search_point_y;
								break;
							case SQUARE_UNKNOWN:
								*x = search_point_x;
								*y = search_point_y+1;
								break;
							default:
								break;
					   }
					}
					if(last_x==*x && last_y==*y) { //Failsafe go back to checkerboard if around the hit point nothing was found!
						mode = 0;
						*x = exit_point_x;
						*y = exit_point_y;
					}
					break;
				case 2: //destroy ship horiziontal-left
					if(last_x>0 && ship_map[last_x][last_y]==SQUARE_HIT && ship_map[last_x-1][last_y]!=SQUARE_EMPTY) {
						*x = last_x-1;
						*y = last_y;
					} else {
						mode = 4;
						if(ship_map[last_x][last_y]==SQUARE_EMPTY) {//return one field when going back!
							*x=last_x+1;
							*y=last_y;
						}
					}
					break;
				case 4: //destroy ship horizontal-right
					if (last_x<MAP_SIZE-1 && ship_map[last_x][last_y]==SQUARE_HIT && ship_map[last_x+1][last_y]!=SQUARE_EMPTY){
						*x = last_x+1;
						*y = last_y;
					} else {
						mode=0;
						*x = exit_point_x;
						*y = exit_point_y;
					}
					break;
				case 3: //destroy ship vertical-up
					if(last_y>0 && ship_map[last_x][last_y]==SQUARE_HIT && ship_map[last_x][last_y-1]!=SQUARE_EMPTY) {
							*x = last_x;
							*y = last_y-1;
					} else {
							mode = 5;
						if(ship_map[last_x][last_y]==SQUARE_EMPTY) {//return one field when going back!
							*x=last_x;
							*y=last_y+1;
						}
					}
					break;
				case 5: //destroy ship vertical-down
						if (last_y<MAP_SIZE-1 && ship_map[last_x][last_y]==SQUARE_HIT && ship_map[last_x][last_y+1]!=SQUARE_EMPTY){
								*x = last_x;
								*y = last_y+1;
						} else {
								mode=0;
								*x = exit_point_x;
								*y = exit_point_y;
						}
						break;
			}
			last_x = *x;
			last_y = *y;
		} while(ship_map[*x][*y]!=SQUARE_UNKNOWN);
		debug_print("mode=%i\n", mode);
	} 
}

/**
 *  @brief Cleanup function. Frees the Address Info struct, Closes sockfd
 */
static void cleanup(void) {
	freeaddrinfo(ai);
	close(sockfd);
}
