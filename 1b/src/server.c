/**
 * @file server.c
 * @author Alexander Halbarth <alexander.halbarth@student.tuwien.ac.at>, OSUE Team <osue-team@cps.tuwien.ac.at>
 * @date 2017-11-04
 *
 * @brief Server for OSUE exercise 1B `Battleship'.
 * @details This is the battleship server application
 * It manages all the ships and the game mechanics
 * The game map is sized 10x10 fields. There are 2 ships  with length 2, 3 with length 3 and one with length 4. Default port is 1280.
 * The game is limited by 80 rounds, after that the server wins, if the client did not shoot all ships. See common.h for more Default values!
 *
 * Communication is established over a network socket.
 * 
 * usage: ./server [-p PORT] SHIP1...
 */

// IO, C standard library, POSIX API, data types:
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <getopt.h>

// Assertions, errors, signals:
#include <assert.h>
#include <errno.h>
#include <signal.h>

// Time:
#include <time.h>

// Sockets, TCP, ... :
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>

// stuff shared by client and server:
#include "common.h" /**< Contains all kinds of shared Constants for client and server, debug flag and a method for printing the ships. */

// constants with colors for terminal output
#include "colors.h"

// Static variables for things you might want to access from several functions:
static const char *port = DEFAULT_PORT; // the port to bind to

// Static variables for resources that should be freed before exiting:
static struct addrinfo *ai = NULL;      /**< addrinfo struct*/
static int sockfd = -1;                 /**< socket file descriptor*/
static int connfd = -1;                 /**< connection file descriptor*/

static char* argv0; /**< The program name */

uint8_t ship_map[MAP_SIZE][MAP_SIZE]; /**< Array to store game field*/

static void prt_usage(void); /**< Print usage Prototype */
static void place_ship(char*); /**< Place Ship Prototype */
static int shot_position(uint8_t); /**< shot position Prototype */
static void cleanup(void); /**< cleanup Prototype*/


/**
 * Program entry point.
 * @brief The program starts here. This function takes care about parameters, places the ships with the place_ship() method.
 * Starts the socket and waits for a connection. When the connection is established it waits for inputs by the client and calls the shot_position() method.
 * After each shot it sends a response to the client according to the return value of shot_position()
 * When the game is finished it performes a clean-up and terminates with EXIT_SUCCESS. 
 * If there are errors it terminates with EXIT_FAILURE prints an error message to stderr and prints out the usage on usage failures.
 * 
 * @details global variables: 
 * argv0 Program name
 * addresinfo Address Information struct for socket
 * sockfd File descriptor of the socket
 * connfd File descriptor of the connection
 * @param argc The argument counter.
 * @param argv The argument vector.
 * @return Returns EXIT_SUCCESS on Sucess and EXIT_FAILURE on Failure.
 */
int main(int argc, char *argv[])
{
	//parse command line arguments
	argv0 = argv[0];
	int c;
	while( (c=getopt(argc,argv, "p:")) != -1) {
                switch(c) {
                        case 'p': // Port specified
                                if(optarg!=NULL) {
                                        port = optarg;
                                } else {
                                        fprintf(stderr,"[%s] ERROR: No Port specified\n",argv0);
                                        prt_usage();
                                        exit(EXIT_FAILURE);
                                }
                                break;
                        default: // Error on wrong Arguments
							prt_usage();
							exit(EXIT_FAILURE);
							break;
                }
        }
	if((argc-optind) != (SHIP_CNT_LEN2+SHIP_CNT_LEN3+SHIP_CNT_LEN4)) {
		fprintf(stderr,"[%s] ERROR: wrong number of ships: %i\n",argv0,(argc-optind));
        exit(EXIT_FAILURE);
	}
	for(int i = optind; i<argc ;i++) { //place ships
		place_ship(argv[i]);
	}
	
	debug_print("%s\n","Game set up:");
	print_map(ship_map);
	debug_print("Starting Server on Port=%s\n",port);

	//setting up the socket
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    int res = getaddrinfo(NULL, port, &hints, &ai); //set socket address in ai
	if(res!=0) {
		fprintf(stderr,"[%s] ERROR: getaddrinfo %s\n",argv0,gai_strerror(res));
        exit(EXIT_FAILURE);
	}

    sockfd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol); //create socket
	if(sockfd<0) {
		cleanup();
		fprintf(stderr,"[%s] ERROR: socket file descriptor: %s\n",argv0,strerror(errno));
        exit(EXIT_FAILURE);
	}

    int val = 1;
    res = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof val); //set socket options
	if(res!=0) {
		cleanup();
		fprintf(stderr,"[%s] ERROR: socket options: %s\n",argv0,strerror(errno));
        exit(EXIT_FAILURE);
	}

    res = bind(sockfd, ai->ai_addr, ai->ai_addrlen);//assign socket to address
	if(res!=0) {
		cleanup();
		fprintf(stderr,"[%s] ERROR: socket bind %s\n",argv0,strerror(errno));
        exit(EXIT_FAILURE);
	}

    res = listen(sockfd, 1); // listen for connections
	if(res!=0) {
		cleanup();
        fprintf(stderr,"[%s] ERROR: socket listen %s\n",argv0,strerror(errno));
        exit(EXIT_FAILURE);
    }
	debug_print("%s\n","Waiting for connection");
    connfd = accept(sockfd, NULL, NULL); //accept new connection and set conn file descriptor
	if(connfd<0) {
		cleanup();
        fprintf(stderr,"[%s] ERROR: connfd %s\n",argv0,strerror(errno));
        exit(EXIT_FAILURE);
    }
	debug_print("%s\n","Client Connected");

	int rounds = 0; //round counter
	
	uint8_t message; // message from client
    while ((res=recv(connfd,&message,sizeof(message),MSG_WAITALL))>0) { //wait for message and go on when return value of recv is OK
		rounds++; //next round starts
		
		debug_print("RECIEVED: %i\n",message);
		
		res = shot_position(message); // shoot to position sent from the client
		debug_print("Round %i\n",rounds);
		print_map(ship_map);

		uint8_t answer; //answer to send to the client
		
		switch(res) { //check return value of shot_position (errors are negative!!)
			case HIT_NOTHING:
				answer = HIT_NOTHING;
				break;
			case HIT_SHIP:
				answer = HIT_SHIP;
				break;
			case HIT_SHIP_SUNK:
				answer = HIT_SHIP_SUNK;
				break;
			case HIT_LAST_SHIP_SUNK:
				answer = HIT_LAST_SHIP_SUNK | (STATUS_GAME_OVER*4); //automatically game over!
				break;
			case -STATUS_PARITY_ERROR:
				answer = STATUS_PARITY_ERROR *4; //Error, but not critical for the server, so no exit!
				//fprintf(stderr,"[%s] %sWARNING: parity error%s\n",argv0,KYEL,KNRM);
				fprintf(stderr,"[%s] %sERROR: parity error%s\n",argv0,KYEL,KNRM);
				rounds--; //decrement rounds since round was invalid!
				break;
			case -STATUS_COORD_ERROR:
				answer = STATUS_COORD_ERROR *4; //Error, but not critical for the server, so no exit!
				//fprintf(stderr,"[%s] %sWARNING: invalid coordinate%s\n", argv0,KYEL,KNRM);
				fprintf(stderr,"[%s] %sERROR: invalid coordinate%s\n", argv0,KYEL,KNRM);
				rounds--; //decrement rounds since round was invalid!
				break;
			default: //default return value, could be reached when shot_position has an error!
				fprintf(stderr,"[%s] %sERROR: shot_position wrong return value %i%s\n",argv0,KRED,res,KNRM);
				cleanup();
				exit(EXIT_FAILURE);
				break;
		}
		if(rounds >= MAX_ROUNDS) { //check if game is over
			answer=answer | (STATUS_GAME_OVER*4); //game over
		}

		if(send(connfd,&answer,sizeof(answer),0)<sizeof(answer)) { //send response to client and check on error!
			cleanup();
			fprintf(stderr,"[%s] ERROR: send %s\n",argv0,strerror(errno));
			exit(EXIT_FAILURE);
		} else {
			debug_print("message=%i sent\n",answer);
		}

		if((answer & (STATUS_GAME_OVER*4))>0 && (answer & 8)==0) { //Status = GAME_OVER
			//char* winner = (((answer & 3)==HIT_LAST_SHIP_SUNK) ? "Client" : "Server");
			//fprintf(stdout,"[%s] %sINFO: GAME OVER. %s won!%s\n",argv0,KGRN,winner,KNRM);
			char* winner = (((answer & 3)==HIT_LAST_SHIP_SUNK) ? "client wins in" : "client lost after"); //print  game over message
			fprintf(stdout,"[%s] %s%s %i rounds%s\n",argv0,KGRN,winner,rounds,KNRM);
			res=0;
			break;
		}
    }

	if(res==0) { //check if termination due to error or end of strem/file
		debug_print("%s\n","doing cleanup");
		cleanup();
		exit(EXIT_SUCCESS);
	} else {//failure:
		cleanup();
		fprintf(stderr,"[%s] ERROR: recv %s\n",argv0,strerror(errno));
        exit(EXIT_FAILURE);	
	} 
	//unreachable code!
	return EXIT_FAILURE;
}


/**
 *  @brief place_ship method. Is used to parse arguments and place the ships according to these arguments.
 *  
 *  @param [in] coord The coord string from the command line argument Example A0A1 for ship from A0 to A1 coordinate
 *  
 *  @details Checks if the placement is correct and places the ship. if there is an error it terminates the program and prints out a message
 */
static void place_ship(char* coord) {
	static int cnt_2 = 0; //Number of ships placed with length 2
	static int cnt_3 = 0; //Number of ships placed with length 3
	static int cnt_4 = 0; //Number of ships placed with length 4
	if(strlen(coord)==4) {
		if(coord[0]-'A'<0 || coord[2]-'A'<0 || coord[1]-'0'<0 || coord[3]-'0'<0 ||
			coord[0]-'A'>=MAP_SIZE || coord[2]-'A'>=MAP_SIZE || coord[1]-'0'>=MAP_SIZE || coord[3]-'0'>=MAP_SIZE) {
			fprintf(stderr,"[%s] ERROR: coordinates outside of map: %s\n",argv0,coord);
			exit(EXIT_FAILURE);
		}
		if(coord[0]==coord[2]) { //vertical placement
			int length = coord[3]-coord[1]+1; //calculate length
			switch(length) { //check if length possible
				case 2:
					if(cnt_2<SHIP_CNT_LEN2) {
						cnt_2++;
					} else {
						fprintf(stderr,"[%s] ERROR: wrong number of ships length 2: %s\n",argv0,coord);
						exit(EXIT_FAILURE);
					}
					break;
				case 3:
						if(cnt_3<SHIP_CNT_LEN3) {
								cnt_3++;
						} else {
								fprintf(stderr,"[%s] ERROR: wrong number of ships length 3: %s\n",argv0,coord);
								exit(EXIT_FAILURE);
						}
						break;
				case 4:
						if(cnt_4<SHIP_CNT_LEN4) {
								cnt_4++;
						} else {
								fprintf(stderr,"[%s] ERROR: wrong number of ships length 4: %s\n",argv0,coord);
								exit(EXIT_FAILURE);
						}
						break;
				default:
					fprintf(stderr,"[%s] ERROR: ship length wrong: %s length=%i\n",argv0,coord,length);
					exit(EXIT_FAILURE);
					break;
			}
			int x = coord[0]-'A';
			for(int i = 0; i<length;i++) {
				int y = coord[1]-'0'+i;
				if(x >0 && ship_map[x-1][y]!=SQUARE_UNKNOWN) {//check one field down
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x-1,y);
					prt_usage();
					exit(EXIT_FAILURE);
				}
				if(x<MAP_SIZE-1 && ship_map[x+1][y]!=SQUARE_UNKNOWN) {//check one field up
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x+1,y);
					prt_usage();
					exit(EXIT_FAILURE);
				}
				if(ship_map[x][y]==SQUARE_UNKNOWN) { //check field to set
					ship_map[x][y] = SQUARE_SHIP; //actually place ship
				} else {
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x,y);
					prt_usage();
					exit(EXIT_FAILURE);
				}
			}
			//check front
			int y = coord[1]-'0'-1;
			if(y>=0) {
				if(x >0 && ship_map[x-1][y]!=SQUARE_UNKNOWN) {//check one field down
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x-1,y);
					prt_usage();
					exit(EXIT_FAILURE);
				}
				if(ship_map[x][y]!=SQUARE_UNKNOWN) {//check front
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x,y);
					prt_usage();
					exit(EXIT_FAILURE);
				}
				if(x<MAP_SIZE-1 && ship_map[x+1][y]!=SQUARE_UNKNOWN) {//check one field up
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x+1,y);
					prt_usage();
					exit(EXIT_FAILURE);
				}
			}
			//check heck
			y = coord[3]-'0'+1;
			if(y<MAP_SIZE) {
				if(x >0 && ship_map[x-1][y]!=SQUARE_UNKNOWN) {//check one field down
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x-1,y);
					prt_usage();
					exit(EXIT_FAILURE);
				}
				if(ship_map[x][y]!=SQUARE_UNKNOWN) {//check front
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x,y);
					prt_usage();
					exit(EXIT_FAILURE);
				}
				if(x<MAP_SIZE-1 && ship_map[x+1][y]!=SQUARE_UNKNOWN) {//check one field up
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x+1,y);
					prt_usage();
					exit(EXIT_FAILURE);
				}
			}
		} else if(coord[1]==coord[3]) { //horizontal placement
			int length = coord[2]-coord[0]+1;
			switch(length) { //check if length possible
					case 2:
							if(cnt_2 < SHIP_CNT_LEN2) {
									cnt_2++;
							} else {
									fprintf(stderr,"[%s] ERROR: wrong number of ships length 2: %s\n",argv0,coord);
									exit(EXIT_FAILURE);
							}
							break;
					case 3:
							if(cnt_3 < SHIP_CNT_LEN3) {
									cnt_3++;
							} else {
									fprintf(stderr,"[%s] ERROR: wrong number of ships length 3: %s\n",argv0,coord);
									exit(EXIT_FAILURE);
							}
							break;
					case 4:
							if(cnt_4 < SHIP_CNT_LEN4) {
									cnt_4++;
							} else {
									fprintf(stderr,"[%s] ERROR: wrong number of ships length 4: %s\n",argv0,coord);
									exit(EXIT_FAILURE);
							}
							break;
					default:
							fprintf(stderr,"[%s] ERROR: ship length wrong: %s length=%i\n",argv0,coord,length);
							exit(EXIT_FAILURE);
							break;
			}
			int y = coord[1]-'0';
			for(int i = 0; i<length;i++) {
				int x = i+coord[0]-'A';
				if(y>0 && ship_map[x][y-1]!=SQUARE_UNKNOWN) {//check one field down
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x,y-1);
					prt_usage();
					exit(EXIT_FAILURE);
				}
				if(y<MAP_SIZE-1 && ship_map[x][y+1]!=SQUARE_UNKNOWN) {//check one field up
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x,y+1);
					prt_usage();
					exit(EXIT_FAILURE);
				}
				if(ship_map[x][y]==SQUARE_UNKNOWN) { //check field to set
					ship_map[x][y] = SQUARE_SHIP; //actually place ship
				} else {
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x,y);
					prt_usage();
					exit(EXIT_FAILURE);
				}
			}
			int x = coord[0]-'A'-1; //check front
			if(x>=0) {
				if(y >0 && ship_map[x][y-1]!=SQUARE_UNKNOWN) {//check one field down
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x,y-1);
					prt_usage();
					exit(EXIT_FAILURE);
				}
				if(ship_map[x][y]!=SQUARE_UNKNOWN) {//check front
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x,y);
					prt_usage();
					exit(EXIT_FAILURE);
				}
				if(y<MAP_SIZE-1 && ship_map[x][y+1]!=SQUARE_UNKNOWN) {//check one field up
					fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x,y+1);
					prt_usage();
					exit(EXIT_FAILURE);
				}
			}
			//check heck
			x = coord[2]-'A'+1;
			if(x<MAP_SIZE) {
					if(y >0 && ship_map[x][y-1]!=SQUARE_UNKNOWN) {//check one field down
							fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x,y-1);
							prt_usage();
							exit(EXIT_FAILURE);
					}
					if(ship_map[x][y]!=SQUARE_UNKNOWN) {//check front
							fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x,y);
							prt_usage();
							exit(EXIT_FAILURE);
					}
					if(y<MAP_SIZE-1 && ship_map[x][y+1]!=SQUARE_UNKNOWN) {//check one field up
							fprintf(stderr,"[%s] ERROR: ship placement wrong: %s other ship at: %c%i\n",argv0,coord,'A'+x,y+1);
							prt_usage();
							exit(EXIT_FAILURE);
					}
			}
		} else { //not horizontal or vertical placement
			fprintf(stderr,"[%s] ERROR: ships must be aligned either horizontally or vertically: %s\n",argv0,coord);
			prt_usage();
            exit(EXIT_FAILURE);
		}
	} else {
		fprintf(stderr,"[%s] ERROR: wrong syntax for ship coordinates: %s\n",argv0,coord);
		prt_usage();
		exit(EXIT_FAILURE);
	}
}

/**
 *  @brief prints the usage of the program to stderr
 *  
 *  @details usage: ./server [-p PORT] SHIP1...
 *   */
static void prt_usage() {
	fprintf(stderr, "usage: %s [-p PORT] SHIP1...\n", argv0);
}

/**
 *  @brief Makes to shot to the position supplied by message
 *  
 *  @param [in] message message from client including parity bit
 *  @return answer to send to client
 *  0  = Nothing was Hit
 *  1  = Hit Ship
 *  2  = Hit Ship and sunk ship with the Hit
 *  3  = Sunk the last remaining ship
 *  -2 = Parity Bit Error
 *  -3 = Coordinate Error (wrong coordinate or multiple shots to same coordinate)
 *  
 *  @details Makes the shot, saves value to the ship_map array and returns the code to send to the client
 */
static int shot_position(uint8_t message) {
	int c = 0;
	for(int i = 1;i<=128;i*=2) { //check parity
		if((i&message)>0) {
			c++;
		}
	}
	if(c%2!=0) { // even parity wrong
		return -STATUS_PARITY_ERROR; // Return wrong parity! No exit since no harm is done!
	} else { //parity correct
		message = message&127;//remove parity bit
	}
	int x = message%10; //x coordinate
	int y = message/10; //y coordinate

	switch(ship_map[x][y]) { //lookup the coordinate the client shot to
		case SQUARE_UNKNOWN:
			ship_map[x][y]=SQUARE_EMPTY;
			return HIT_NOTHING;
			break;
		case SQUARE_HIT:
		case SQUARE_EMPTY:
			return -STATUS_COORD_ERROR; // Return wrong coordinate since double shot to the same field!
			break;
		case SQUARE_SHIP:
			//check ship_sunk:
			if(( x>0 && ship_map[x-1][y]==SQUARE_SHIP)||( x<MAP_SIZE-1 && ship_map[x+1][y]==SQUARE_SHIP) ||
				( x>0 && ship_map[x-1][y]==SQUARE_HIT)||( x<MAP_SIZE-1 && ship_map[x+1][y]==SQUARE_HIT)) { //ship horizontal
				int x2 = x-1; //search left
				while(x2 >= 0 && ship_map[x2][y]!=SQUARE_UNKNOWN && ship_map[x2][y]!=SQUARE_EMPTY) {
					if(ship_map[x2][y]==SQUARE_SHIP) {
						ship_map[x][y]=SQUARE_HIT;
			                        return HIT_SHIP;
					}
					x2--;
				}
				x2 = x+1; //search right
				while(x2<MAP_SIZE && ship_map[x2][y]!=SQUARE_UNKNOWN && ship_map[x2][y]!=SQUARE_EMPTY) {
						if(ship_map[x2][y]==SQUARE_SHIP) {
								ship_map[x][y]=SQUARE_HIT;
								return HIT_SHIP;
						}
					x2++;
				}
			} else if(( y>0 && ship_map[x][y-1]==SQUARE_SHIP)||( y<MAP_SIZE-1 && ship_map[x][y+1]==SQUARE_SHIP) ||
                                ( y>0 && ship_map[x][y-1]==SQUARE_HIT)||( y<MAP_SIZE-1 && ship_map[x][y+1]==SQUARE_HIT)) {//ship vertical
				int y2 = y-1; //search up
				while(y2>=0 && ship_map[x][y2]!=SQUARE_UNKNOWN && ship_map[x][y2]!=SQUARE_EMPTY) {
						if(ship_map[x][y2]==SQUARE_SHIP) {
								ship_map[x][y]=SQUARE_HIT;
								return HIT_SHIP;
						}
					y2--;
				}

				y2 = y+1; //search down
				while(y2 < MAP_SIZE && ship_map[x][y2]!=SQUARE_UNKNOWN && ship_map[x][y2]!=SQUARE_EMPTY) {
						if(ship_map[x][y2]==SQUARE_SHIP) {
								ship_map[x][y]=SQUARE_HIT;
								return HIT_SHIP;
						}
					y2++;
				}
			} else {
				fprintf(stderr,"[%s] ERROR: ship size 1 in map\n",argv0);
				cleanup();
	            exit(EXIT_FAILURE);
			}
			//ship sunk! check for LAST_SHIP_SUNK!
			for(int i = 0; i<MAP_SIZE;i++) {
				for(int j = 0; j<MAP_SIZE;j++) {
					if(!(x==i && y==j) && ship_map[i][j]==SQUARE_SHIP) {
						ship_map[x][y]=SQUARE_HIT;
						return HIT_SHIP_SUNK;
					}
				}
			}
			ship_map[x][y]=SQUARE_HIT;
			return HIT_LAST_SHIP_SUNK;
			break;
		default:
			fprintf(stderr,"[%s] ERROR: wrong value in ship_map[%i][%i]=%i\n",argv0,x,y,ship_map[x][y]);
			cleanup();
			exit(EXIT_FAILURE);
			break;
	}
	//unreachable code
	return -9;
}

/**
 *  @brief Cleanup function. Frees the Address Info struct, Closes connfd and sockfd
 */
static void cleanup(void) {
	freeaddrinfo(ai);
	close(connfd);
	close(sockfd);
}
