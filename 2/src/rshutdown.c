/**
 * @file rshutdown.c
 * @author Alexander Halbarth 01129193 <alexander.halbarth@student.tuwien.ac.at>
 * @date 2017-11-28
 *
 * @brief Randsched rshutdown for OSUE exercise 2
 * @details Outputs "SHUTDOWN COMPLETED" in 1 out of 3 cases and exits with EXIT_SUCCESS
 * in 2 out of 3 cases it ouputs "KaBOOM!" end exits with EXIT_FAILURE
 */

#include "rnd.h"
#include <stdio.h>
#include <stdlib.h>

/**
 *  @brief The Main method of the program
 *  
 *  @param argc The argument counter.
 *  @param argv The argument vector.
 *  @return Returns EXIT_SUCCESS on Sucess and EXIT_FAILURE on Failure.
 *  
 *  @details Executes the functionality of the program
 */
int main(int argc, char **argv)
{
	if(((int) (rnd()*3)) < 1) { // 0
		printf("SHUTDOWN COMPLETED\n");
		return EXIT_SUCCESS;
	} else {
		printf("KaBOOM!\n");
		return EXIT_FAILURE;
	}
}

