/**
 * @file rventgas.c
 * @author Alexander Halbarth 01129193 <alexander.halbarth@student.tuwien.ac.at>
 * @date 2017-11-28
 *
 * @brief Randsched rventgas for OSUE exercise 2
 * @details Outputs "STATUS OK" 6 out of 7 cases and exits with EXIT_SUCCESS
 * on 1 out of 7 cases it ouputs "PRESSURE TOO HIGH - IMMEDIATE SHUTDOWN REQUIRED" end exits with EXIT_FAILURE
 */
 
#include "rnd.h"
#include <stdio.h>
#include <stdlib.h>

/**
 *  @brief The Main method of the program
 *  
 *  @param argc The argument counter.
 *  @param argv The argument vector.
 *  @return Returns EXIT_SUCCESS on Sucess and EXIT_FAILURE on Failure.
 *  
 *  @details Executes the functionality of the program
 */
int main(int argc, char **argv)
{
	if(((int) (rnd()*7)) < 6) { // 0 to 7
		printf("STATUS OK\n");
		return EXIT_SUCCESS;
	} else {
		printf("PRESSURE TOO HIGH - IMMEDIATE SHUTDOWN REQUIRED\n");
		return EXIT_FAILURE;
	}
}

