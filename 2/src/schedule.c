/**
 * @file schedule.c
 * @author Alexander Halbarth 01129193 <alexander.halbarth@student.tuwien.ac.at>
 * @date 2017-11-28
 *
 * @brief Randsched scheduler for OSUE exercise 2
 * @details This is the scheduler application
 * It starts the specified program repetitive until it fails and then starts emergency once
 * The ouput from the program and the success of the emergency program is written into the logfile
 * 
 * Synopsis:
 * schedule [-s <seconds>] [-f <seconds>] <program> <emergency> <logfile>
 * -s <seconds> Time Window Start (Default: 1 Second)
 * -f <seconds> max. Time Window end (Default: 0 Second)
 * <program> Program to be executed repetitive
 * <emergency> Program which should be executed on failure
 * <logfile> Path to file to log output from <program> to and Success and Failure of <emergency> program
 */

#include "colors.h"
#include "rnd.h"
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <signal.h>


static char* argv0; /**< The Program name*/

#define PIPE_R 0 /**< Read End of Pipe */
#define PIPE_W 1 /**< Write End of Pipe */

#define BUF_SIZE 50 /**< Size of Read/Write Buffer */

#define DEBUG 0 /**< Debug flag to toggle debug output 0=off 1=on*/

/** Debug printing Macro source: https://stackoverflow.com/a/1644898 */
#define debug_print(fmt, ...) \
        do { if (DEBUG) fprintf(stdout, "%s[#%i]: %s:%d:%s(): DEBUG: " fmt, argv0, getpid(), __FILE__, \
                                __LINE__, __func__, __VA_ARGS__); } while (0)

/** Error printing Macro */
#define error_print(fmt, ...) \
        fprintf(stderr, KRED "%s[#%i]: %s:%d:%s(): ERROR: " fmt KNRM, argv0, getpid(), __FILE__, \
                                __LINE__, __func__, __VA_ARGS__);

/**
 * @brief This function writes helpful usage information about the program to stderr.
 * @details global variables: argv0
 */
static void prt_usage(void) {
	fprintf(stderr, "%s [-s <seconds>] [-f <seconds>] <program> <emergency> <logfile> \n-s <seconds>\tZeitfenster Anfang (Default: 1 Sekunde) \n-f <seconds>\tmax. Zeitfenster Dauer (Default: 0 Sekunden)\n<program>\tProgramm inkl. Pfad, das wiederholt ausgefuehrt werden soll\n<emergency>\tProgramm inkl. Pfad, das im Fehlerfall ausgefuehrt wird\n<logfile>\tPfad zu einer Datei, in der die Ausgabe von <program> sowie Erfolg/Misserfolg von <emergency> protokolliert werden\n", argv0);
}

/**
 * Signal handler for Interupt handling prototype.
 * See void handle_signal(int signal)
 **/
void handle_signal(int);

static FILE* logfile; /**< The Stream I/O Struct for the Logfile*/

static short killflag=0; /**< Flag to indicate if the program should be terminated */

/**
 *  @brief The Main method of the program
 *  
 *  @param argc The argument counter.
 *  @param argv The argument vector.
 *  @return Returns EXIT_SUCCESS on Sucess and EXIT_FAILURE on Failure.
 *  
 *  @details Executes the functionality of the program and returns EXIT_SUCCESS when finished regardless of the return value of the child programs.
 *  It returns EXIT_FAILURE when there is a failure in this program. It can be closed with SIGINT and SIGTERM then it terminates safely wit EXIT_FAILURE
 */
int main(int argc, char **argv)
{
	argv0 = argv[0]; // Set Program Name
	int c;
	int wait = 1;
	int arb_wait = 0;
	char* program;
	char* emergency;
	char* logfilename;
	while( (c=getopt(argc,argv, "s:f:")) != -1) {
		switch(c) {
			case 's': // wait time
				if(wait==1) {
					char* a; // characters after number
					wait=strtol(optarg, &a, 10);
					if(wait <= 0 || *a != '\0') {
						error_print("Invalid input for wait time: %s",optarg);
						prt_usage();
						exit(EXIT_FAILURE);
					}
				} else {
					error_print("%s","Wait time multiple set!");
					prt_usage();
					exit(EXIT_FAILURE);
				}
				break;
			case 'f': // arb_wait time 
				if(arb_wait==0) {
					char* a; // characters after number
					arb_wait=strtol(optarg, &a, 10);
					if(arb_wait < 0 || *a != '\0') {
						error_print("Invalid input for wait arbitariation time: %s\n",optarg);
						prt_usage();
						exit(EXIT_FAILURE);
					}
				} else {
					error_print("%s\n","Wait time arbitariation multiple set!");
					prt_usage();
					exit(EXIT_FAILURE);
				}
				break;
			default:
				error_print("Undefined Option: %c\n",c);
				prt_usage();
				exit(EXIT_FAILURE);
				break;
		}
	}
	if((argc-optind) != 3) { // 3 arguments supplied?
		error_print("Wrong number of arguments supplied: %i\n", argc-optind);
		prt_usage();
 		exit(EXIT_FAILURE);
	}
	program = argv[0+optind]; //The program to be executed
	emergency = argv[1+optind]; // The program to be executed on failure
	logfilename = argv[2+optind]; // The name of the Logfile
	debug_print("wait=%i arb_wait=%i\n",wait, arb_wait);

	logfile = fopen(logfilename, "w");
	if(logfile==NULL) {
		error_print("fopen(logfilename) failed: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	struct sigaction sa; //Installation of the Signal Handler on SIGINT, SIGTERM
	sa.sa_handler = handle_signal;
	if(sigaction(SIGINT, &sa, NULL)<0) {
		error_print("Error on sigaction SIGINT: %s\n",strerror(errno));
		exit(EXIT_FAILURE);
	}
	if(sigaction(SIGTERM, &sa, NULL)<0) {
		error_print("Error on sigaction SIGTERM: %s\n",strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	int ret;
	do {
		unsigned int slp = wait + (int) (rnd()*(arb_wait+1));
		debug_print("sleeping for %i seconds\n", slp);
		while(slp > 0)
			slp = sleep(slp);
		debug_print("Create Pipe for communication%s\n","");
		int pipefd[2];
		int i = pipe(pipefd);
		if(i < 0 || pipefd[PIPE_R]<3 || pipefd[PIPE_W]<3) {
			error_print("Error on pipe creation: %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
		fflush(logfile); //Needed because of fork!
		fflush(stdout); //Needed because of fork!
		debug_print("Starting child process %s\n", "");
		int chld = fork();
		if(chld<0) {
			error_print("Error on fork: %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		} else if(chld==0) { // in child process
			fclose(logfile); // logfile not needed anymore!
			close(pipefd[PIPE_R]); // close read end of pipe
			debug_print("Setting write-end pipe to stdout in child process. pipefd=%i\n", pipefd[PIPE_W]);
			if(dup2(pipefd[PIPE_W],STDOUT_FILENO)<0) { // configure input of pipe instead of stdout
				error_print("Error on dup2 of pipe into stdout: %s\n",strerror(errno));
				exit(EXIT_FAILURE);
			}
			close(pipefd[PIPE_W]); //not needed anymore
			execlp(program, program, NULL); // Start program 1st parameter should be split at first space!
			close(STDOUT_FILENO); // Only reached on error!
			error_print("Error on execlp(program): %s\n",strerror(errno));
			exit(EXIT_FAILURE);
		}
		close(pipefd[PIPE_W]); //close write end of pipe
		
		//Read Output of child into STDOUT and Logfile
		char buf[BUF_SIZE]; // Buffer for Reading from Pipe
		FILE* pipe = fdopen(pipefd[PIPE_R], "r"); // Pipe Read End Strem I/O Stuct
		if(pipe == NULL) {
			error_print("Error on opening pipe with stream I/O: %s\n",strerror(errno));
			exit(EXIT_FAILURE);
		}
		debug_print("Reading from pipe into stdout & log. pipefd=%i\n", pipefd[PIPE_R]);
		while(feof(pipe) == 0) {
			char* c = fgets(buf, sizeof(buf), pipe); //Read pipe content into buffer
            if((c==NULL && feof(pipe)==0) || ferror(pipe) != 0) {
				error_print("Error on reading from pipe with stream I/O: %s\n",strerror(errno));
				exit(EXIT_FAILURE);
			}
			fputs(buf, stdout); // Write Buffer to Stdout
			if(ferror(stdout) != 0) {
				error_print("Error on writing buffer to stdout: %s\n",strerror(errno));
				exit(EXIT_FAILURE);
			}
			fputs(buf, logfile); // Write Buffer into Logfile
			if(ferror(logfile) != 0) {
				error_print("Error on writing buffer to logfile: %s\n",strerror(errno));
				exit(EXIT_FAILURE);
			}
			buf[0]='\0'; // Failsave when read does something strange! Protects from multiple writes.
		}
		fclose(pipe); //Not needed anymore
		fflush(logfile); //Needed because of duplicate written stuff
		fflush(stdout); //Needed because of duplicate written stuff

		int status;
		if(waitpid(chld,&status,0) < 0) { // Wait for termination of child process
			fclose(logfile);
			error_print("Error on waitpid: %s\n",strerror(errno));
			exit(EXIT_FAILURE);
		}
		chld = -1;
		ret = WEXITSTATUS(status); // Get Exit Status of child process
	} while(ret==EXIT_SUCCESS && killflag==0); // Execute until program fails or Interupt
	
	if(killflag==0) { //check if process should be terminated
		// Error on Child Process, start emergency program
		int chld = fork();
		if(chld<0) {
			error_print("Error on fork: %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		} else if(chld==0) { // in child process
			fclose(logfile); //not needed!
			execlp(emergency, emergency, NULL);
			error_print("Error on execlp(program): %s\n",strerror(errno));
			exit(EXIT_FAILURE); // unreachable!
		}
		int status;
		if(waitpid(chld,&status,0) < 0) {
			fclose(logfile);
			error_print("Error on waitpid: %s\n",strerror(errno));
			exit(EXIT_FAILURE);
		}
		chld = -1;
		ret = WEXITSTATUS(status); // Get Exit Status of child process
		if(ret == EXIT_SUCCESS) { // Exit successful
			fprintf(stdout, "EMERGENCY SUCCESSFUL\n");
			fprintf(logfile, "EMERGENCY SUCCESSFUL\n");
			fclose(logfile); //close Logfile
			exit(EXIT_SUCCESS);
		} else { // Exit not successful
			fprintf(stdout, "EMERGENCY FAILURE\n");
			fprintf(logfile, "EMERGENCY FAILURE\n");
			fclose(logfile); //close Logfile
			exit(EXIT_SUCCESS);
		}
		//unreachable!
		exit(EXIT_FAILURE);
	}

	debug_print("Terminate because of Signal!%s\n","");

	fflush(stdout);
    fflush(logfile);
    fclose(logfile);

	return EXIT_FAILURE;	
}

/**
 *  @brief Signal Handler for SIGINT, SIGTERM to close the program
 *  
 *  @param [in] signal Signal Number
 *  
 *  @details Closes the Application and Clears everything upon exit
 *  
 *  global variables: 
 *  argv0 Program name
 *  logfile
 *  chld Child Process ID
 */
void handle_signal(int signal) {
	if (signal == SIGINT || signal == SIGTERM) {
		killflag=1;
	}
}

