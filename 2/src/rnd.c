/**
 * @file rnd.c
 * @author Alexander Halbarth 01129193 <alexander.halbarth@student.tuwien.ac.at>
 * @date 2017-11-28
 *
 * @brief random number Generator.
 * @details Generates Random Numbers
 */
 
#include "rnd.h"
#include <time.h>
#include <stdlib.h>

/**
 * @brief Creates random double from 0 to 1 with rand and time function.
 * @return Returns Random Double from 0 to 1
 */
double rnd(void) {
        srand(time(NULL));
        return ((double) rand() / (double) RAND_MAX);
}
