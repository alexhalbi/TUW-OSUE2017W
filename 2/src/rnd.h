/**
 * @file rnd.h
 * @author Alexander Halbarth 01129193 <alexander.halbarth@student.tuwien.ac.at>
 * @date 2017-11-28
 *
 * @brief random number Generator.
 * @details Generates Random Numbers
 */
 
/**
* @brief Creates random double from 0 to 1 with rand and time function.
* @return Returns Random Double from 0 to 1
*/
double rnd(void);
