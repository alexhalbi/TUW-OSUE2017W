/**
 * @file mrna-server.h
 * @author Alexander Halbarth 01129193 <alexander.halbarth@student.tuwien.ac.at>
 * @date 08.01.2018
 *
 * @brief MRNA-Server header file of mRNA. Used for De And Encoding Codons into Proteins sent by a client over a shared memory. Synchronization is established via Semaphores.
 * 
 **/

 /**
  *  Inclusion of comm.h since it is needed in clientdatastruct
  */
#include "comm.h"

/**
 *  Maximum Number of clients connected to the server concurrently
 */
#define MAX_CLIENTS_ALLOWED_UNTIL_WAIT 100

/**
 *  Struct with client Data for saving it on the server
 *  
 *  @var pid The pid of the client
 *  @var seq[] the mRNA Sequence of the client
 *  @var seq_length the length of the seq[] of the client
 *  @var cursor the currrent cursor position of the client.
 */
struct clientdatastruct {
     unsigned int pid;
     short seq[SHM_BUF_SIZE]; /**< The mRNA Sequence encoded in one short per Nuclein */
     int seq_length; /**< The length of the mRNA sequence in seq[] */
     int cursor; /**< The cursor on which represents the position in the seq[] array of the server*/
};

