/**
 * @file dict.h
 * @author Alexander Halbarth 01129193 <alexander.halbarth@student.tuwien.ac.at>
 * @date 05.01.2018
 *
 * @brief Dictionary header module of mRNA.
 * 
 * This Header file contains the declarations for the Dictionary to de- and encrypt of the Input and all Methods to search the Dictionary Entries in the C-File dict.c
 **/

#define PROT_BUF_SIZE 10	/**< Buffer Size for Protein String Copy Buffer */
#define START_CODE 0		/**< Protein Start Code */
#define STOP_CODE 1		/**< Protein STOP Code */

const int RNA_LENGTH; /**< Size of RNA Dictionary used for Itaration through it.*/

const int PROT_LENGTH; /**< Size of Protein Dictionary used for Itaration through it.*/

const int RNA_PROT_LENGTH; /**< Size of RNA - Protein Dictionary used for Itaration through it.*/


/**
 * @brief Searches for fitting RNA Character from RNA Number.
 *
 * @param [in] num RNA Number which should be decoded.
 * @return RNA Character which fits the RNA Number
 *
 * @details Returns Matching Entry from Array RNA. Returns -1 if nothing is found.
 */
char getRNAFromNum(short);

/**
 * @brief Searches for fitting RNA Number for RNA Character.
 *
 * @param [in] num RNA Character which should be encoded.
 * @return RNA Number which fits the RNA Character
 *
 * @details Iterates with loop through Array RNA and searches for matching entry. If there are duplicates, it returns the entry with the lowest i
ndex. Returns -1 if nothing is found.
 */
short getNumFromRNA(char);

/**
 * @brief Searches for fitting Protein String from Protein Number.
 *
 * @param [in] num Protein Number which should be decoded.
 * @return Protein String which fits the Protein Number
 *
 * @details Returns Matching Entry from Dictionary Array PROT. Returns NULL if nothing is found.
 */
void getProtFromNum(short,char*);

/**
 * @brief Searches for fitting Number for Protein String.
 *
 * @param [in] c Protein String which should be encoded.
 * @return Protein Number which fits the Protein String
 *
 * @details Iterates with loop through Dictionary Array PROT and searches for matching entry. If there are duplicates, it returns the entry with the lowest i
ndex. Returns -1 if nothing is found. The search is performed Case-insensitive.
 */
short getNumFromProt(char*);

/**
 * @brief Searches for fitting Protein Character from Number.
 *
 * @param [in] num Protein Number which should be decoded.
 * @return Protein Character which fits the Protein Number
 *
 * @details Returns Matching Entry from Dictionary Array PROT. Returns -1 if nothing is found.
 */
char getProtShortFromNum(short);

/**
 * @brief Searches for fitting Number from Protein Character.
 *
 * @param [in] c Protein Character which should be encoded.
 * @return Protein Number which fits the Protein Character
 *
 * @details Iterates with loop through Dictionary Array PROT and searches for matching entry. If there are duplicates, it returns the entry with the lowest i
ndex. Returns -1 if nothing is found.
 */
short getNumFromProtShort(char);

/**
 * @brief Encodes the Codon as a short from a 4 character String representing the RNA Nucleins.
 *
 * @param [in] cod Codon as a 4 character String
 * @return Number which represents the Codon
 *
 * @details Creates with the help of getNumFromRNA the Codon from the given number. Returns -1 on Error.
 */
short getNumFromCodon(char*);

/**
 * @brief Decodes the Codon as a 4 Character String from a Number made with getNumFromCodon()
 *
 * @param [in] num Encoded Codon
 * @return 4 Sized character Array which represents the Codon
 *
 * @details Creates with the help of getRNAFromNum the Codon from the given number. Returns -1 on Error.
 */
void getCodonFromNum(short,char*);

/**
 * @brief Searches for fitting Protein from given RNA.
 *
 * @param [in] rna RNA Number which should be searched.
 * @return Protein Number which fits the RNA
 *
 * @details Returns Matching Entry from Dictionary Array RNA_PROT. Returns -1 on error.
 */
short getProtFromRNA(short);

/**
 * @brief Searches for first RNA from given Protein.
 *
 * @param [in] prot Protein Number which should be searched.
 * @return RNA Number which fits the Protein (First in List if multiple!)
 *
 * @details Iterates with loop through Dictionary Array RNA_PROT and searches for matching entry. If there are duplicates, it returns the entry with the lowe
st index. Returns -1 if nothing is found.
 */
short getRNAFromProt(short);
 
/**
 *  @brief Creates an encoded Codon from 3 RNA Numbers
 *  
 *  @param [in] short First RNA
 *  @param [in] short Second RNA
 *  @param [in] short Third RNA
 *  @return Codon encoded as short usable by dict.c
 */
short getCodonNumFromRNANum(short,short,short);
