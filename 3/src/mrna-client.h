/**
 * @file mrna-client.h
 * @author Alexander Halbarth 01129193 <alexander.halbarth@student.tuwien.ac.at>
 * @date 07.01.2018
 *
 * @brief MRNA-Client header File module of mRNA. Contains Messages and constants used for Client - Human interaction.
 **/
 
#define COM_SUBM 's' /**< Submit Command */
#define COM_NEXT 'n' /**< Next Command */
#define COM_REST 'r' /**< Reset Command */
#define COM_QUIT 'q' /**< Quit Command */

/**
 *  Command help Text of the Program
 */
#define COMMANDS "Available commands:\n\t" \
    "s - submit a new mRNA sequence\n\t" \
    "n - show next protein sequence in active mRNA sequence\n\t" \
    "r - reset active mRNA sequence\n\t" \
    "q - close this client\n"

#define NEW_COMMAND "Enter new command. (Enter to end input): " /**< New Command Prompt */

#define WRONG_COMMAND "Invalid command %c\n" COMMANDS /**< Invalid Command Prompt. Automatically add COMMANDS */

#define NEW_RNA "Enter new mRNA sequence. (Newline to end input):\n" /**< mRNA Prompt */

#define NO_RNA_MSG "No mRNA sequence read in. Send s to submit a new mRNA sequence.\n" /**< No mRNA Sequence submittet at server Error Message */

#define SEQ_FOUND "Protein sequence found [%i/%i] to [%i/%i]: %s\n" /**< Protein Sequence found Message */
#define END_SEQ "End reached [%i/%i], send r to reset.\n" /**< End of mRNA Sequence reached Message */

#define REST_MSG "Reset. [%i/%i]\n" /**< Reset Cursor Message */

#define CLOSE_MSG "Close client.\n" /**< Close Client Message */

#define MAX_WAIT 17 /**< Maximum Wait Time for Server Semaphore to be created */

#define SERVER_FULL_MSG "Server capacities full!\n" /**< Message when server cannot take mor clients */

#define INVALID_MRNA_MSG "Invalid input for mRNA!\n" /**< Message when invalid mRNA is put into the client */

#define SERVER_UNAVAIL_MSG "Server unavailable. Retrying in %isec\n" /**< Message when Server is unavailable with Seconds */

#define TERMINATING_MSG "Terminating, as requested!\n" /**< Terminating Message when waiting for Server to start */

