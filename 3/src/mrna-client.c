/**
 * @file mrna-client.c
 * @author Alexander Halbarth 01129193 <alexander.halbarth@student.tuwien.ac.at>
 * @date 05.01.2018
 *
 * @brief MRNA-Client program module of mRNA. Used to send and receive Data from the Server over a shared memory. Synchronization is established via Semaphores.
 * 
 **/

#include "mrna-client.h"
#include "dict.h"
#include "comm.h"

#include <string.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>

char* argv0; /**< The Program Name*/

sem_t *sfree; /**< Semaphore which shows if the server is busy with a client or not */
sem_t *sem_client; /**< Semaphore which shows if the a command is available to compute for the server */
sem_t *sem_server;  /**< Semaphore which shows if the result from the server is ready for the client */
int shmfd; /**< Filedescriptor of the shared Memory Portion */
struct myshm *shared; /** Shared Memory struct used for Data Exchange */
short exit_flag =0; /**< Exit flag which gets set to show the program that it should exit. */

/**
 *  @brief Method prototype of cleanup function
 *  
 *  See void cleanup(void) for more details
 */
static void cleanup(void);

/**
 *  @brief Method prototype of semaphore wait function
 *  
 *  See void void semaphore_wait(sem_t *s) for more details
 */
static void semaphore_wait(sem_t*,short,short);

/**
 * @brief Signal handler for Interrupt handling prototype.
 * See void handle_signal(int signal) for more details
 **/
static void handle_signal(int);

/**
 *  @brief The Main method of the program
 *  
 *  @param argc The argument counter.
 *  @param argv The argument vector.
 *  @return Returns EXIT_SUCCESS on Success and EXIT_FAILURE on Failure.
 *  
 *  @details Executes the functionality of the program and returns EXIT_SUCCESS when finished.
 *  It returns EXIT_FAILURE when there is a failure in this program. It can be closed with SIGINT and SIGTERM then it terminates safely.
 */
int main(int argc, char **argv) {
	argv0 = argv[0];
    //Register atexit Method
    if(atexit(cleanup)!=0) {
        error_print("Cleanup Method could not be set atexit: %s", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    struct sigaction sa; //Installation of the Signal Handler on SIGINT, SIGTERM
    sa.sa_handler = handle_signal;
    if(sigaction(SIGINT, &sa, NULL)<0) {
        error_print("Error on sigaction SIGINT: %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(sigaction(SIGTERM, &sa, NULL)<0) {
        error_print("Error on sigaction SIGTERM: %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(sigaction(SIGSRVKILLED, &sa, NULL)<0) {
        error_print("Error on sigaction SIGSRVKILLED: %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }

	//Open Semaphores
    sfree = sem_open(SEM_FREE,0);
    if(errno==ENOENT && sfree == SEM_FAILED) {
        for(int i = 1; i < MAX_WAIT && (errno==ENOENT && sfree == SEM_FAILED); i = i*2) {
            printf(SERVER_UNAVAIL_MSG,i);
            if(sleep(i)>0) {
                printf(TERMINATING_MSG);
                exit(EXIT_SUCCESS);
            }
        	sfree = sem_open(SEM_FREE,0);
        }
    }
	if(sfree == SEM_FAILED || sfree == NULL) {
	    error_print("Error at opening Free Semaphore: %s %i\n",strerror(errno),errno);
   		exit(EXIT_FAILURE);
	}
	debug_print("Waiting for connection...%s\n","");
    semaphore_wait(sfree,0,1); //Wait for connection to be available before continuing with stuff the server might not have created

    sem_client = sem_open(SEM_WAITING,0);
	if(sem_client == SEM_FAILED) {
		error_print("Error at opening Client Semaphore: %s\n",strerror(errno));
                exit(EXIT_FAILURE);
    }
	sem_server  = sem_open(SEM_OUTPUT_READY, 0);
	if(sem_server == SEM_FAILED) {
		error_print("Error at opening Server Semaphore: %s\n",strerror(errno));
                exit(EXIT_FAILURE);
    }
	//Open Shared Memory
	shmfd = shm_open(MEMORY_NAME, O_RDWR, PERMISSION);

	if(shmfd < 0) {
		error_print("Error at opening Shared Memory: %s\n",strerror(errno));
		exit(EXIT_FAILURE);
	}

	shared = mmap(NULL, sizeof *shared, PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);

	if (shared == MAP_FAILED) {
		error_print("Error at opening Mapping SHM: %s\n",strerror(errno));
		exit(EXIT_FAILURE);
	}
    
    debug_print("%s\n","Waiting for Server");
    semaphore_wait(sem_client,0,1); // Wait for Server to be available

     if(exit_flag == 1) {
         if(sem_post(sem_client)<0) { // Reset Semaphore before Exit!
             error_print("Error at resetting sem_server: %s\n",strerror(errno));
             exit(EXIT_FAILURE);
         }
         exit(EXIT_SUCCESS);
     }

     //Initiate Connection to Server
     shared->mode = STATE_HELLO;
     shared->pid = getpid();
     shared->length = 0;
     shared->data[0] = SHM_TERM_BIT;

     if(sem_post(sem_server)<0) { //Data written to SHM -> Server should take actions
         error_print("Error at resetting sem_server: %s\n",strerror(errno));
         exit(EXIT_FAILURE);
     }
     debug_print("%s","Waiting for Server Reply\n");
     semaphore_wait(sem_client,1,0); //SHM wait for response
     if(shared->mode == STATE_SUCC) {
         debug_print("%s","Connection initiated!\n");
     } else if(shared->mode == STATE_ERR) {
         if(shared->data[0] == ERR_FULL) {
             printf(SERVER_FULL_MSG);
             exit(EXIT_FAILURE);
         } else {
             error_print("Error at Server! No.:%i\n",shared->data[0]);
         }
     } else {
         error_print("Protocol Error at Submit or Error at Server! State=%i\n",shared->mode);
         exit(EXIT_FAILURE);
     }
    
     if(sem_post(sem_client)<0) { //SHM is now free for other clients
         error_print("Error at resetting sem_client: %s\n",strerror(errno));
         exit(EXIT_FAILURE);
     }

     if(sem_post(sfree)<0) { // Reset sfree Semaphore new connection initializations are possible!
         error_print("Error at resetting sfree: %s\n",strerror(errno));
         exit(EXIT_FAILURE);
     }

     printf(COMMANDS);

    //read command:
    do{
        printf(NEW_COMMAND);
        fflush(stdout);
        char com[2]; //1 Command, 1 Newline
        short nl = 0; //Terminating flag for Read
		for(int pos=0; pos < sizeof(com) && nl==0;) { //Read until buffer full or terminating flag is set
            int numread = read(STDIN_FILENO,com,sizeof(com) - pos);
            if(numread < 0) {
                if(errno == EINTR) {
                    debug_print("%s","Interrupt recieved while read!\n");
                    exit_flag = 1;
                    break;
                }
                error_print("Error at Read of stdin: %s\n",strerror(errno));
                exit(EXIT_FAILURE);
            }
            for(int i = 0; i<numread; i++) { //Check for \n and \0
                if(com[pos+i] == '\n' || com[pos+i] == '\0') {
                    nl = 1;
                }
            }
            pos += numread;
        }
        if(exit_flag == 1)//Do not go to switch on exit_flag
            break;

        if(com[1]!='\n') {//Command Invalidation on wrong input!
            com[0]=0;
        }

        switch(com[0]) {
            case COM_SUBM: //Submit new RNA Seq to Server
                printf(NEW_RNA);
                char in[SHM_BUF_SIZE];
                short nl = 0;//Terminating flag for Read
                for(int pos=0; pos < sizeof(in) && nl==0;) { //Read until buffer full or terminating flag is set
                    int numread = read(STDIN_FILENO,in,sizeof(in) - pos);
                    if(numread < 0) {
                        if(errno == EINTR) {
                            debug_print("%s","Interrupt recieved while read!\n");
                            exit_flag = 1;
                            break;
                        }
                        error_print("Error at Read of stdin: %s\n",strerror(errno));
                        exit(EXIT_FAILURE);
                    }
                    for(int i = 0; i<numread; i++) { //Check for \n and \0
                         if(in[pos+i] == '\n' || in[pos+i] == '\0') {
                             nl = 1;
                         }
                     }
                    pos += numread;
                }
                if(exit_flag == 1)
                    break;
                debug_print("%s\n","Waiting until SHM is free");
                semaphore_wait(sem_client,0,0); //Wait before writing Data to SHM
                shared->mode = STATE_INPUT;
                int i;
                for(i=0;i<SHM_BUF_SIZE;i++) {
                    if(in[i]=='\0' || in[i]=='\n' || in[i]=='\0') {
                        break;
                    }
                    shared->data[i]=getNumFromRNA(in[i]);
                }
                shared->length=i;
                shared->data[shared->length] = SHM_TERM_BIT;
                shared->seq_length=shared->length;
                shared->pid = getpid();
                debug_print("Sending new RNA Sequence: [%s] length=%i\n",in,i);
                if(sem_post(sem_server)<0) { //Data written to SHM -> Server should take actions
                    error_print("Error at resetting sem_server: %s\n",strerror(errno));
                    exit(EXIT_FAILURE);
                }
                debug_print("%s","Waiting for Server Reply\n");
                semaphore_wait(sem_client,1,0); //SHM wait for response
                if(shared->mode == STATE_SUCC) {
                    debug_print("%s","Server Response OK!\n");
                } else if(shared->mode == STATE_ERR) {
                    if(shared->data[0] == ERR_NO_DATA) {
                        printf(INVALID_MRNA_MSG);
                    } else {
                        error_print("Error at Server! No.:%i\n",shared->data[0]);
                    }
                } else {
                    error_print("Protocol Error at Submit or Error at Server! State=%i\n",shared->mode);
                }
                if(sem_post(sem_client)<0) { //SHM is now free for other clients
                    error_print("Error at resetting sem_client: %s\n",strerror(errno));
                    exit(EXIT_FAILURE);
                }
                break;
            case COM_NEXT: //Query next Protein from Server
                semaphore_wait(sem_client,0,0); //Wait before Writing Command to SHM
                shared->mode = STATE_NEXT;
                shared->data[0] = SHM_TERM_BIT;
                shared->length=0;
                shared->pid = getpid();
                debug_print("%s","Sending new NEXT Protein Command\n");
                if(sem_post(sem_server)<0) { //Server should now look into the SHM
                    error_print("Error at resetting sem_server: %s\n",strerror(errno));
                    exit(EXIT_FAILURE);
                }
                debug_print("%s","Waiting for Server Reply\n");
                semaphore_wait(sem_client,1,0);
                switch(shared->mode) {
                    case STATE_END: //No more Proteins in RNA
                        printf(END_SEQ,shared->cursor1,shared->seq_length);
                        break;
                    case STATE_ERR: //Error on Server
                        if(shared->data[0] == ERR_NO_DATA) {
                            printf(NO_RNA_MSG);
                        } else {
                            error_print("Error on Server in Next Protein: %i\n",shared->data[0]);
                        }
                        break;
                    case STATE_OUTPUT:; //Output supplied by Server
                        char seq[SHM_BUF_SIZE];
                        int i = 0;
                        for(; i< shared->length && shared->data[i] != SHM_TERM_BIT; i++) {
                            seq[i] = getProtShortFromNum(shared->data[i]);
                        }
                        seq[i] = '\0';
                        printf(SEQ_FOUND,shared->cursor1,shared->seq_length,shared->cursor2,shared->seq_length,seq);
                        break;
                    default: // ERROR
                        error_print("Protocol Error at Next or Error at Server! mode=%i\n",shared->mode);
                        break;
                }
                if(sem_post(sem_client)<0) { //SHM now free for other client action!
                    error_print("Error at resetting sem_server SEM: %s\n",strerror(errno));
                    exit(EXIT_FAILURE);
                }
                break;
            case COM_REST: //Reset Cursor on Server
                semaphore_wait(sem_client,0,0); //Wait before Writing Command to SHM
                shared->mode = STATE_RESET;
                shared->data[0] = SHM_TERM_BIT;
                shared->length = 0;
                shared->cursor1 = 0;
                shared->cursor2 = 0;
                shared->pid = getpid();
                if(sem_post(sem_server)<0) { //Send command to Server
                    error_print("Error at resetting sem_server SEM: %s\n",strerror(errno));
                    exit(EXIT_FAILURE);
                }
                semaphore_wait(sem_client,1,0); //Waiting for server reply
                if(shared->mode != STATE_SUCC) {
                    error_print("%s","Protocol Error at Reset");
                }
                printf(REST_MSG,shared->cursor1,shared->seq_length);
                if(sem_post(sem_client)<0) {
                    error_print("Error at resetting sem_client SEM: %s\n",strerror(errno));
                    exit(EXIT_FAILURE);
                }
                break;
            case COM_QUIT: //Quit
                exit_flag = 1;
                break;
            default: //Wrong Command!
                printf(WRONG_COMMAND,com[0]);
                break;
        }
    } while(exit_flag == 0);

    debug_print("%s\n","Exiting");
    //Exit handling:
    semaphore_wait(sem_client,0,0); //Wait SHM to be accessible!
    shared->mode = STATE_TERM;
    shared->data[0] = SHM_TERM_BIT;
    shared->length = 0;
    shared->pid = getpid();
    if(sem_post(sem_server)<0) { //Send command to server
        error_print("Error at resetting sem_server at Exit: %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }

    printf(CLOSE_MSG);
    exit(EXIT_SUCCESS);
}

/**
 *  @brief Cleanup of all created SHM or SEM
 *  
 *  @details Cleanup Method which closes and deletes the Semaphores and the Shared Memory when they are already created.
 */
static void cleanup(void) {
    if(sfree != NULL && sfree != SEM_FAILED) {
        if(sem_close(sfree)<0) {
            error_print("Error on Closing sfree: %s\n",strerror(errno));
        }
    }
    if(sem_client != NULL && sem_client != SEM_FAILED) {
         if(sem_close(sem_client)<0) {
             error_print("Error on Closing sem_client: %s\n",strerror(errno));
         }
    }
    if(sem_server != NULL && sem_server != SEM_FAILED) {
        if(sem_close(sem_server)<0) {
            error_print("Error on Closing sem_server: %s\n",strerror(errno));
        }
    }
    if(shmfd>0) {
        if(close(shmfd)<0) {
              error_print("Error on Closing SHM FD: %s\n",strerror(errno));
        }
        if(munmap(shared, sizeof *shared) <0) {
              error_print("Error on SHM Unmap: %s\n",strerror(errno));
        }
    }
}

/**
 *  @brief Semaphore Wait wrapper with signal handler
 *  
 *  @param [in] s Semaphore Pointer
 *  @param [in] tog_pid Toggles the check if the semaphore waited for is for the current pid. If not the wait is continued!
 *  @param [in] tog_ef Toggles the ignoring of exit_flag. On 1 the exit_flag will bypass this function!
 *  
 *  @details Waits until the semaphore is reset. Sets the exit_flag when an Interrupt happens. Does not wait when exit_flag is already set!
 */
static void semaphore_wait(sem_t *s,short tog_pid,short tog_ef) {
    if(exit_flag == 1 && tog_ef==1) //Do NOT wait when Program should exit!
        return;
    int r;
    do {
        r = sem_wait(s); //Waiting for semaphore
        if(tog_pid==1) {
            if(r==0 && (shared->pid != getpid())) {
                sem_post(s);
                continue;
           }
        }
    } while(r<0 && (tog_ef==1 || errno == EINTR));
            //errno == EINTR);//(tog_ef==1 || errno == EINTR));
    if(r<0) {
        if(errno == EINTR) { //Set EXIT Flag on Interrupt!
            exit_flag = 1;
            return;
        }
        error_print("Error while Waiting for Semaphore: %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }
}

/**
  *   @brief Signal Handler for SIGINT, SIGTERM, SIGSRVKILLED to close the program
  *
  *   @param [in] signal Signal Number
  *
  *   @details Closes the Application and Clears everything upon exit
  *   SIGSRVKILLED will directly perform a cleanup and an _exit, since the server will delete the SHM and SEM anyways in a matter of seconds.
  *
  *   global variables:
  *   argv0 Program name
  *   logfile
  *   chld Child Process ID
  **/
 static void handle_signal(int signal) {
     if (signal == SIGINT || signal == SIGTERM) {
         exit_flag=1;
     } else if (signal == SIGSRVKILLED) {
        cleanup();
        _exit(EXIT_SUCCESS);
     }
 }
