/**
 * @file comm.h
 * @author Alexander Halbarth 01129193 <alexander.halbarth@student.tuwien.ac.at>
 * @date 05.01.2018
 *
 * @brief Communication header module of mRNA.
 *
 * This Header file contains the definitions for Variables which are needed for communication by the server and the client.
 **/

#include "colors.h"

#define PREFIX "01129193_" /**< Prefix for all Semaphores and Shared Memory Files */
#define MEMORY_NAME "/" PREFIX "mrna_shm" /**< Name of the Shared Memory File */
#define PERMISSION (0600) /**< Permissions used for SHM and SEM*/

#define SEM_FREE "/" PREFIX "sem_free"		/**< Semaphore Name which shows if the server is busy with a client or not */
#define SEM_WAITING "/" PREFIX "sem_wait"	/**< Semaphore Name which shows if the a command is available to compute for the server */
#define SEM_OUTPUT_READY "/" PREFIX "sem_out"	/**< Semaphore Name which shows if the result from the server is ready for the client */

#define STATE_INPUT	    1  /**< Mode of myshm which tells that mRNA Input is in myshm */
#define STATE_OUTPUT	2  /**< Mode of myshm which tells that Protein Output is in myshm */
#define STATE_RESET     3  /**< Mode of myshm which tells that Curser should be reset */
#define STATE_NEXT      4  /**< Mode of myshm which tells that Next Proteins got requested */
#define STATE_SUCC      10 /**< Mode of myshm which tells that Server Action succsseeded (Reset + Input) */
#define STATE_HELLO     20 /**< Mode of myshm which tells the Server that a new client is there */
#define STATE_END       60 /**< Mode of myshm which tells that mRNA Sequence ended */
#define STATE_ERR       62 /**< Mode of myshm which tells that the Server has an Error. Error Information is stored in data[0] */
#define STATE_TERM	    63 /**< Mode of myshm which tells that the Client Terminated */

#define ERR_NO_DATA     -2 /**< Error Code of myshm which tells, togehter with STATE_ERR that there is no sequence in the server to calculate next Protein. Error Codes are stored in data[0] */
#define ERR_FULL        -3 /**< Error Code of myshm which tells, togehter with STATE_ERR that there is no more capacities for more clients.
 Error Codes are stored in data[0] */

#define DEBUG 0 /**< Debug flag to toggle debug output 0=off 1=on*/

/** Debug printing Macro source: https://stackoverflow.com/a/1644898 */
#define debug_print(fmt, ...) \
        do { if (DEBUG) fprintf(stdout, "%s: %s:%d:%s(): DEBUG: " fmt, argv0, __FILE__, \
                                __LINE__, __func__, __VA_ARGS__); } while (0)

/** Error printing Macro */
#define error_print(fmt, ...) \
        fprintf(stderr, KRED "%s: %s:%d:%s(): ERROR: " fmt KNRM, argv0, __FILE__, \
                                __LINE__, __func__, __VA_ARGS__);

#define SHM_BUF_SIZE 100 /**< Buffer Size of the Shared Memory Data Array. This Buffer is also the maximum input size of the client and the maximum protein output size, which cannot be bigger than the input anyways.**/

#define SHM_TERM_BIT -1 /**< Terminating Bit for data array */

#define SIGSRVKILLED SIGUSR1 /**< Signal Number to kill all client when server exits! */

/**
 *  Shared Memory struct which represents the data structure in the Shared Memory Part.
 *  
 *  @var pid The pid of the client
 *  @var mode Mode defined through STATE_*
 *  @var data[] Data saved in Shared Memory
 *  @var length Length of the data saved in data[]
 *  @var seq_length needed for output on client
 *  @var cursor1 Cursor variable needed for output on client (Reset Position, Start Sequence Position)
 *  @var cursor2 Cursor variable needed for output on client (Reset Position, End Sequence Position)
 */
struct  myshm {
    unsigned int pid;
	unsigned short mode;
	short data[SHM_BUF_SIZE];
    int length;
    int seq_length;
    int cursor1;
    int cursor2;
};
