/**
 * @file mrna-server.c
 * @author Alexander Halbarth 01129193 <alexander.halbarth@student.tuwien.ac.at>
 * @date 05.01.2018
 *
 * @brief MRNA-Server program module of mRNA. Used for De And Encoding Codons into Proteins sent by a client over a shared memory. Synchronization is established via Semaphores.
 * 
 **/

#include "mrna-server.h" //includes comm.h
#include "dict.h"

#include <string.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

char* argv0; /**< The Program Name*/

sem_t *sfree; /**< Semaphore which shows if the server is busy with a client or not */
sem_t *sem_client; /**< Semaphore which shows if the a command is available to compute for the server */
sem_t *sem_server;  /**< Semaphore which shows if the result from the server is ready for the client */
int shmfd; /**< Filedescriptor of the shared Memory Portion */
struct myshm *shared; /** Shared Memory struct used for Data Exchange */

/**
 *  @brief Method prototype of cleanup function
 *  
 *  See void cleanup(void) for more details
 */
static void cleanup(void);

/**
 *  @brief Method prototype of semaphore wait function
 *  
 *  See void void semaphore_wait(sem_t *s) for more details
 */
static void semaphore_wait(sem_t*);

/**
 * @brief Signal handler for Interrupt handling prototype.
 * See void handle_signal(int signal) for more details
 **/
static void handle_signal(int);

short exit_flag =0; /**< Exit flag which gets set to show the program that it should exit. */

/**
 *  Struct Array with Client Data. Maximum Client Count is MAX_CLIENTS_ALLOWED_UNTIL_WAIT
 */
struct clientdatastruct clientdata[MAX_CLIENTS_ALLOWED_UNTIL_WAIT];

/**
 *  @brief Retrives Client Data from clientdata[] array
 *  
 *  @details For more details see static struct clientdatastruct* getClientData(int pid)
 */
static struct clientdatastruct* getClientData(int);

/**
 *  @brief The Main method of the program
 *  
 *  @param argc The argument counter.
 *  @param argv The argument vector.
 *  @return Returns EXIT_SUCCESS on Success and EXIT_FAILURE on Failure.
 *  
 *  @details Executes the functionality of the program and returns EXIT_SUCCESS when finished.
 *  It returns EXIT_FAILURE when there is a failure in this program. It can be closed with SIGINT and SIGTERM then it terminates safely.
 */
int main(int argc, char **argv) {
	argv0 = argv[0];
    //Register atexit Method
    if(atexit(cleanup)!=0) {
        error_print("Cleanup Method could not be set atexit: %s", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    struct sigaction sa; //Installation of the Signal Handler on SIGINT, SIGTERM
    sa.sa_handler = handle_signal;
    if(sigaction(SIGINT, &sa, NULL)<0) {
        error_print("Error on sigaction SIGINT: %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(sigaction(SIGTERM, &sa, NULL)<0) {
        error_print("Error on sigaction SIGTERM: %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }

	//Open Semaphores
	sfree = sem_open(SEM_FREE,O_CREAT | O_EXCL, PERMISSION, 0);
    if(sfree == SEM_FAILED) {
        error_print("Error at opening sfree Semaphore: %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }
	sem_client = sem_open(SEM_WAITING,O_CREAT | O_EXCL, PERMISSION, 1);
    if(sem_client == SEM_FAILED) {
        error_print("Error at opening Client Semaphore: %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }
	sem_server  = sem_open(SEM_OUTPUT_READY,O_CREAT | O_EXCL, PERMISSION, 0);
	if(sem_server == SEM_FAILED) {
         error_print("Error at opening Server Semaphore: %s\n",strerror(errno));
         exit(EXIT_FAILURE);
    }
	//Open Shared Memory
	shmfd = shm_open(MEMORY_NAME, O_RDWR | O_CREAT, PERMISSION);

	if(shmfd < 0) {
		exit(EXIT_FAILURE);
	}

	if(ftruncate(shmfd, sizeof *shared) <0) {
		exit(EXIT_FAILURE);
	}

	shared = mmap(NULL, sizeof *shared, PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);

	if (shared == MAP_FAILED)
		exit(EXIT_FAILURE);

    if(sem_post(sfree)<0) { // Open sfree to allow connection
        error_print("Error at resetting sfree SEM: %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }
	
    do {
        debug_print("%s","SEM_WAIT sem_server\n");
        semaphore_wait(sem_server); //Wait for smth to do
        debug_print("Recieved new Command=%i from %i\n",shared->mode,shared->pid);
        if(exit_flag==1) 
            break;
        struct clientdatastruct *cd = getClientData(shared->pid);
        if(shared->mode != STATE_HELLO && cd == NULL) {
            error_print("Client Error with pid=%i!\n",shared->pid);
            exit(EXIT_FAILURE);
        }
        if(exit_flag==1)
            break;
        switch(shared->mode){
            case STATE_INPUT: //New Sequence from client recieved
                debug_print("Input recieved starting with: %i\n",shared->data[0]);
                if(shared->length<1) {
                    error_print("Invalid length supplied by client = %i\n",shared->length);
                    shared->mode = STATE_ERR;
                    shared->data[0] = ERR_NO_DATA;
                    shared->data[1] = SHM_TERM_BIT;
                    shared->length = 1;
					shared->seq_length = 0;

                    cd->seq_length = 0;
                    for(int i=0;i < SHM_BUF_SIZE; i++) {
                       cd->seq[i] = SHM_TERM_BIT;
                    }
                } else {
                    int i = 0;
                    int j = 0;
                    for(i = 0, j = 0; i < SHM_BUF_SIZE && j < shared->length && shared->length <= SHM_BUF_SIZE; i++, j++) {
                        if(shared->data[j]>=0 && shared->data[j]<=3) {
                            cd->seq[i] = shared->data[j];
                        } else {
                            i--;
                        }
                    }
                    cd->seq_length = i;
                    if(cd->seq_length > 0) {
                        for(;i < SHM_BUF_SIZE; i++) {
                            cd->seq[i] = SHM_TERM_BIT;
                        }
    
                        if(DEBUG) { // Debug output of Sequence added to Memory
                            debug_print("New Sequence added to Server:[%s","");
                            int k = 0;
                            for(k = 0;k < cd->seq_length && cd->seq[k] >= 0; k++) {
                                printf("%c",getRNAFromNum(cd->seq[k]));
                            }
                            printf("] length=%i\n",cd->seq_length);
                        }
                        cd->cursor = 0;
	                    shared->mode = STATE_SUCC;
    	                shared->data[0] = SHM_TERM_BIT;
        	            shared->length = 0;
                    } else {
						error_print("Invalid length supplied by client = %i\n",shared->length);
            	        shared->mode = STATE_ERR;
        	            shared->data[0] = ERR_NO_DATA;
    	                shared->data[1] = SHM_TERM_BIT;
	                    shared->length = 1;
						shared->seq_length = 0;
						
						cd->seq_length = 0;
						for(int i=0;i < SHM_BUF_SIZE; i++) {
                             cd->seq[i] = SHM_TERM_BIT;
                        }
                    }
                }
                if(sem_post(sem_client)<0) { //Send Response
                    error_print("Error at resetting sem_client SEM: %s\n",strerror(errno));
                    exit(EXIT_FAILURE);
                }
                debug_print("%s","Sent response\n");
                break;
            case STATE_NEXT: //Client requests the next Protein
                if(cd->seq_length<1) {
                    debug_print("Invalid Sequence Length supplied! %i\n",cd->seq_length);
                    shared->mode = STATE_ERR;
                    shared->data[0] = ERR_NO_DATA;
                    shared->data[1] = SHM_TERM_BIT;
                    shared->length = 1;
                } else {
                    for(;cd->cursor < cd->seq_length-2;cd->cursor++) {
                        debug_print("Using Codon: %c%c%c\n",getRNAFromNum(cd->seq[cd->cursor]),getRNAFromNum(cd->seq[cd->cursor+1]),getRNAFromNum(cd->seq[cd->cursor+2]));
                        short cod = getCodonNumFromRNANum(cd->seq[cd->cursor],cd->seq[cd->cursor+1],cd->seq[cd->cursor+2]);
                        short prot = getProtFromRNA(cod);
                            if(prot==START_CODE || cd->seq[cd->cursor]==SHM_TERM_BIT) {
                                break;
                            }
                    }
                    if(cd->cursor >= cd->seq_length-2 || cd->seq[cd->cursor]==SHM_TERM_BIT) {
                        debug_print("%s","No Start Code found! -> End of Sequence\n");
                        cd->cursor = cd->seq_length;
                        shared->cursor1 = cd->cursor;
                        shared->cursor2 = cd->cursor;
                        shared->seq_length = cd->seq_length;
                        shared->mode = STATE_END;
                        shared->data[0] = SHM_TERM_BIT;
                        shared->length = 0;
                    } else {
                        debug_print("Start Code at %i\n",cd->cursor);
                        cd->cursor += 2;
                        shared->mode = STATE_OUTPUT;
                        shared->cursor1 = cd->cursor-2; //START incl Start Codon 0-based
                        cd->cursor++;
                        int i;
                        shared->data[0] = START_CODE; //Add Start Code to mRNA
                        for(i=1;cd->cursor<cd->seq_length-2 && cd->seq[cd->cursor]!=SHM_TERM_BIT;cd->cursor = cd->cursor+3,i++) {
                            short cod = getCodonNumFromRNANum(cd->seq[cd->cursor],cd->seq[cd->cursor+1],cd->seq[cd->cursor+2]);
                            short prot = getProtFromRNA(cod);
                            if(prot==STOP_CODE) {
                                debug_print("Stop Code at %i\n",cd->cursor);
                                cd->cursor += 2;
                                break;
                            }
                            shared->data[i] = prot;
                            debug_print("Add to Sequence: '%c'=%i\n",getProtShortFromNum(shared->data[i]),shared->data[i]);
                        }
                        if(cd->cursor < cd->seq_length-2 && cd->seq[cd->cursor]!=SHM_TERM_BIT) {
                            shared->cursor2=cd->cursor-2; //Before STOP Codon 0-based
                            shared->length = i;
                            shared->data[i]=SHM_TERM_BIT;
                            shared->seq_length = cd->seq_length;
                        } else {
                            cd->cursor = cd->seq_length;
                            if(i<1) {
                                debug_print("%s","No Protein found! -> End of Sequence\n");
                                shared->seq_length = cd->seq_length;
                                shared->cursor1 = cd->cursor;
                                shared->cursor2 = cd->cursor;
                                shared->mode = STATE_END;
                                shared->data[0] = SHM_TERM_BIT;
                                shared->length = 0;
                            } else {
                                shared->cursor2 = cd->cursor;
                                shared->length = i;
                                shared->data[i]=SHM_TERM_BIT;
                                shared->seq_length = cd->seq_length;
                            }
                        }
                    }
                }
                if(sem_post(sem_client)<0) { //Send response
                    error_print("Error on sem_post of sem_client: %s\n",strerror(errno));
                    exit(EXIT_FAILURE);
                }
                debug_print("%s\n","Response sent!");
                break;
            case STATE_RESET: //Curser Reset to Position 0
                debug_print("%s\n","Resetting cursor!");
                cd->cursor = 0;
                shared->mode = STATE_SUCC;
                shared->data[0] = SHM_TERM_BIT;
                shared->seq_length = cd->seq_length;
                shared->cursor1 = cd->cursor;
                shared->cursor2 = cd->cursor;
                shared->length = 0;
                if(sem_post(sem_client)<0) {
                    error_print("Error on sem_post of sem_client: %s\n",strerror(errno));
                    exit(EXIT_FAILURE);
                }
                debug_print("%s\n","Response sent!");
                break;
            case STATE_TERM: //Client Termintated, Cleanup and getting ready for new client!
                debug_print("%s\n","Client Quit!");
                cd->seq_length=0;
                for(int i=0;i<SHM_BUF_SIZE;i++) {
                    cd->seq[i] = SHM_TERM_BIT;
                }
                cd->cursor = 0;
                cd->pid = 0;

                // Client now gone!
                if(sem_post(sem_client)<0) { //Wait for client command
                    error_print("Error on sem_post of sem_client: %s\n",strerror(errno));
                    exit(EXIT_FAILURE);
                }
                break;
            case STATE_HELLO: //New Client wants to connect!
                if(shared->pid>0) {
                    int i;
                    for(i = 0; i < MAX_CLIENTS_ALLOWED_UNTIL_WAIT; i++) {
                        if(clientdata[i].pid<1) {
                            cd = &clientdata[i];
                            break;
                        }
                    }
                    if(i>=MAX_CLIENTS_ALLOWED_UNTIL_WAIT) { //No more connections allowed
                        shared->mode = STATE_ERR;
                        shared->data[1] = SHM_TERM_BIT;
                        shared->data[0] = ERR_FULL;
                        shared->length = 1;
                        debug_print("Client #%i could not connect. No more connections allowed",shared->pid);
                    } else { // Create Data
                        cd->pid = shared->pid;
                        cd->cursor = 0;
                        cd->seq_length = 0;
                        for(int i=0; i<SHM_BUF_SIZE;i++) {
                            cd->seq[i] = SHM_TERM_BIT;
                        }
                        shared->mode = STATE_SUCC;
                        shared->data[0] = SHM_TERM_BIT;
                        shared->length = 0;
                        debug_print("Client Data succ created!%s\n","");
                    }
                } else {//Error with PID!
                    shared->mode = STATE_ERR;
                    shared->data[0] = SHM_TERM_BIT;
                    shared->length = 1;
                    debug_print("%s","Client connect error. PID not set correct?\n");
                }
                if(sem_post(sem_client)<0) { //Wait for client command
                    error_print("Error on sem_post of sem_server: %s\n",strerror(errno));
                    exit(EXIT_FAILURE);
                }
                break;
            default: //Invalid command from Client recieved. No exit since it will not impact the server program! Is also called when exit flag terminates the program
                if(exit_flag==0) 
                    error_print("Invalid Mode Supplied! mode=%i\n",shared->mode);
                break;
        }
    } while(exit_flag==0);

    exit(EXIT_SUCCESS);
}

/**
 *  @brief Cleanup of all created SHM or SEM. And closing of clients with Signal
 *  
 *  @details Cleanup Method which closes and deletes the Semaphores and the Shared Memory when they are already created. Sends SIGSRVKILLED to all connected Clients to signal them to close themselves.
 */
static void cleanup(void) {
    //kill all clients!
    
    for(int i = 0; i < MAX_CLIENTS_ALLOWED_UNTIL_WAIT; i++) {
        if(clientdata[i].pid>0) {
            kill(clientdata[i].pid,SIGSRVKILLED);
        }
    }

    //cleanup
    if(sfree != NULL && sfree != SEM_FAILED) {
        if(sem_close(sfree)<0) {
            error_print("Error on Closing sfree: %s\n",strerror(errno));
        }
        if(sem_unlink(SEM_FREE)<0) {
            error_print("Error on Unlinking SEM_FREE: %s\n",strerror(errno));
        }
    }
    if(sem_client != NULL && sem_client != SEM_FAILED) {
        if(sem_close(sem_client)<0) {
            error_print("Error on Closing sem_client: %s\n",strerror(errno));
        }
        if(sem_unlink(SEM_WAITING)<0) {
            error_print("Error on Unlinking SEM_WAITING: %s\n",strerror(errno));
        }
    }
    if(sem_server != NULL && sem_server != SEM_FAILED) {
        if(sem_close(sem_server)<0) {
            error_print("Error on Closing sem_server: %s\n",strerror(errno));
        }
        if(sem_unlink(SEM_OUTPUT_READY)<0) {
            error_print("Error on Unlinking SEM_OUTPUT_READY: %s\n",strerror(errno));
        }
    }
    if(shmfd>0) {
        if(close(shmfd)<0) {
            error_print("Error on Closing SHM FD: %s\n",strerror(errno));
        }
        if(munmap(shared, sizeof *shared) <0) {
            error_print("Error on SHM Unmap: %s\n",strerror(errno));
        }
        if(shm_unlink(MEMORY_NAME) <0) {
            error_print("Error on SHM Unlink: %s\n",strerror(errno));
        }
    }
}

/**
 *  @brief Semaphore Wait wrapper with signal handler
 *  
 *  @param [in] s Semaphore Pointer
 *  
 *  @details Waits until the semaphore is reset. Sets the exit_flag when an Interrupt happens. Does not wait when exit_flag is already set to terminate the program!
 */
static void semaphore_wait(sem_t *s) {
    if(exit_flag == 1) //Do NOT wait when Program should exit!
        return;
    int r;
    do {
        r = sem_wait(s); //Waiting for semaphore
    } while(r<0 && errno != EINTR);
    if(r<0) {
        if(errno == EINTR) { //Set EXIT Flag on Interrupt!
            exit_flag = 1;
            return;
        }
        error_print("Error while Waiting for Semaphore: %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }
}

/**
 *   @brief Signal Handler for SIGINT, SIGTERM to close the program
 *   
 *   @param [in] signal Signal Number
 *   
 *   @details Sets the exit_flag on SIGINT, SIGTERM
 **/
static void handle_signal(int signal) {
    if (signal == SIGINT || signal == SIGTERM) {
        exit_flag=1;
    }
}

/**
 *  @brief retrives Client data from clientdata[i] array via the pid
 *  
 *  @param [in] pid The pid of the client
 *  @return Returns a pointer to a clientdatastruct with the data of the pid or NULL when not found
 *  
 *  @details Returns also NULL when pid<1
 */
static struct clientdatastruct* getClientData(int pid) {
    if(pid<1)
        return NULL;
    for(int i = 0; i < MAX_CLIENTS_ALLOWED_UNTIL_WAIT; i++) {
        if(clientdata[i].pid == pid) {
            return &clientdata[i];
        }
    }
    return NULL;
}
