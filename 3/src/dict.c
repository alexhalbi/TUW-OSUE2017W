/**
 * @file dict.c
 * @author Alexander Halbarth 01129193 <alexander.halbarth@student.tuwien.ac.at>
 * @date 05.01.2018
 *
 * @brief Dictionary program module of mRNA. Used for De And Encoding RNA Characters, Proteins and Codons.
 * 
 * This Module contains the Dictionary to de- and encrypt RNA Sequences to the Numbers used to save them.
 **/

#include "dict.h"
#include <string.h>
#include <stdio.h>

/**
 *  Dictionary definition Array of RNA Struct.
 *  RNA Number is Index.
 *  Statically filled
 */
static const char RNA[] = {'U','C','A','G'};

/**
 *  Dictionary definition Array of Protein Struct.
 *  Prot Number is the Index
 *  Statically filled
 */
static const char* PROT[] = {
"Met(M)", //0 = START
"STOP",  //1
"Phe(F)",//2
"Leu(L)",//3
"Ser(S)",//4
"Tyr(Y)",//5
"Cys(C)",//6
"Trp(W)",//7
"Pro(P)",//8
"His(H)",//9
"Gln(Q)",//10
"Arg(R)",//11
"Ile(I)",//12
"Thr(T)",//13
"Asn(N)",//14
"Lys(K)",//15
"Val(V)",//16
"Ala(A)",//17
"Asp(D)",//18
"Glu(E)",//19
"Gly(G)",//20
};

/**
 *  Dictionary definition Array of Protein RNA-Codon Mapping.
 *  Codon Number is the Index.
 *  Statically filled
 */
static const short RNA_PROT[] = {
2, //0
2, //1
3, //2
3, //3
4, //4
4, //5
4, //6
4, //7
5, //8
5, //9
1, //10
1, //11
6, //12
6, //13
1, //14
7, //15
3, //16
3, //17
3, //18
3, //19
8, //20
8, //21
8, //22
8, //23
9, //24
9, //25
10,//26
10,//27
11,//28
11,//29
11,//30
11,//31
12,//32
12,//33
12,//34
0, //35
13,//36
13,//37
13,//38
13,//39
14,//40
14,//41
15,//42
15,//43
4, //44
4, //45
11,//46
11,//47
16,//48
16,//49
16,//50
16,//51
17,//52
17,//53
17,//54
17,//55
18,//56
18,//57
19,//58
19,//59
20,//60
20,//61
20,//62
20 //63
};

const int RNA_LENGTH = sizeof(RNA)/sizeof(RNA[0]); /**< Size of RNA Dictionary used for Itaration through it.*/

const int PROT_LENGTH = sizeof(PROT)/sizeof(PROT[0]); /**< Size of Protein Dictionary used for Itaration through it.*/

const int RNA_PROT_LENGTH = sizeof(RNA_PROT)/sizeof(RNA_PROT[0]); /**< Size of RNA - Protein Dictionary used for Itaration through it.*/


/**
 * @brief Searches for fitting RNA Character from RNA Number.
 *
 * @param [in] num RNA Number which should be decoded.
 * @return RNA Character which fits the RNA Number
 *
 * @details Returns Matching Entry from Array RNA. Returns -1 if nothing is found.
 */
char getRNAFromNum(short num) {
	if(num<0 || num>=RNA_LENGTH) {
		return -1;
	}
	return RNA[num];
}

/**
 * @brief Searches for fitting RNA Number for RNA Character.
 *
 * @param [in] num RNA Character which should be encoded.
 * @return RNA Number which fits the RNA Character
 *
 * @details Iterates with loop through Array RNA and searches for matching entry. If there are duplicates, it returns the entry with the lowest i
ndex. Returns -1 if nothing is found.
 */
short getNumFromRNA(char c) {
	for(int i = 0; i<RNA_LENGTH;i++) {
                if(RNA[i] == c) {
                        return i;
                }
        }
        return -1;
}

/**
 * @brief Searches for fitting Protein String from Protein Number.
 *
 * @param [in] num Protein Number which should be decoded.
 * @param [out] prot Pointer to write Protein String into. Size should be minimum PROT_BUF_SIZE! Filled with strcpy.
 * @return Protein String which fits the Protein Number into prot Pointer
 *
 * @details Returns Matching Entry from Dictionary Array PROT. Returns NULL if nothing is found.
 */
void getProtFromNum(short num, char* prot) {
	if(num<0 || num>=PROT_LENGTH) {
		prot = NULL;
	}
	strcpy(prot,PROT[num]);
}

/**
 * @brief Searches for fitting Number for Protein String.
 *
 * @param [in] c Protein String which should be encoded.
 * @return Protein Number which fits the Protein String
 *
 * @details Iterates with loop through Dictionary Array PROT and searches for matching entry. If there are duplicates, it returns the entry with the lowest index. Returns -1 if nothing is found. The search is performed Case-insensitive.
 */
short getNumFromProt(char* c) {
        for(int i = 0; i<PROT_LENGTH;i++) {
                if(PROT[i] == c) {
                        return i;
                }
        }
        return -1;
}

/**
 * @brief Searches for fitting Protein Character from Number.
 *
 * @param [in] num Protein Number which should be decoded.
 * @return Protein Character which fits the Protein Number
 *
 * @details Returns Matching Entry from Dictionary Array PROT. Returns -1 if nothing is found.
 */
char getProtShortFromNum(short num) {
	if(num<0 || num>=PROT_LENGTH) {
                return -1;
        }
        return PROT[num][4];
}

/**
 * @brief Searches for fitting Number from Protein Character.
 *
 * @param [in] c Protein Character which should be encoded.
 * @return Protein Number which fits the Protein Character
 *
 * @details Iterates with loop through Dictionary Array PROT and searches for matching entry. If there are duplicates, it returns the entry with the lowest index. Returns -1 if nothing is found.
 */
short getNumFromProtShort(char c) {
        for(int i = 0; i<PROT_LENGTH;i++) {
                if(PROT[i][4] == c) {
                        return i;
                }
        }
        return -1;
}

/**
 * @brief Encodes the Codon as a short from a 3 character String representing the RNA Nucleins.
 *
 * @param [in] cod Codon as a 3 character String (Excluding \0 Termination which is needed as cod[3]!)
 * @return Number which represents the Codon
 *
 * @details Creates with the help of getNumFromRNA the Codon from the given number. Returns -1 on Error.
 */
short getNumFromCodon(char* cod) {
	if(strlen(cod)!=3) {
		fprintf(stderr,"dict.c: Wrong String (%s) Supplied!",cod);
        return -1;
	}
    return getCodonNumFromRNANum(getNumFromRNA(cod[0]),getNumFromRNA(cod[1]),getNumFromRNA(cod[2]));
}

/**
 * @brief Decodes the Codon as a 4 Character String from a Number made with getNumFromCodon()
 *
 * @param [in] num Encoded Codon
 * @param [out] *cod Decoded Codon Character Pointer with minimum Size 5 (including Terminating Bit!)
 * @return 4 Sized character Array which represents the Codon into *cod
 *
 * @details Creates with the help of getRNAFromNum the Codon from the given number. Returns NULL on Error.
 */
void getCodonFromNum(short num,char* cod) {
	for(int i = 0; i<4;i++) {
                char rna = getRNAFromNum(num&(4<<(i*2)));
                if(rna<0) {
                        cod = NULL;
                }
		cod[i]=rna;
        }
}

/**
 * @brief Searches for fitting Protein from given RNA.
 *
 * @param [in] num RNA Number which should be searched.
 * @return Protein Number which fits the RNA
 *
 * @details Returns Matching Entry from Dictionary Array RNA_PROT. Returns -1 on error.
 */
short getProtFromRNA(short num) {
	if(num<0 || num>=RNA_PROT_LENGTH) {
		return -1;
	}
	return RNA_PROT[num];
}

/**
 * @brief Searches for first RNA from given Protein.
 * 
 * @param [in] prot Protein Number which should be searched.
 * @return RNA Number which fits the Protein (First in List if multiple!)
 *
 * @details Iterates with loop through Dictionary Array RNA_PROT and searches for matching entry. If there are duplicates, it returns the entry with the lowest index. Returns -1 if nothing is found.
 */
short getRNAFromProt(short prot) {
        for(int i = 0; i<RNA_PROT_LENGTH;i++) {
                if(RNA_PROT[i] == prot) {
                        return i;
                }
        }
        return -1;
}

/**
 *  @brief Creates an encoded Codon from 3 RNA Numbers
 *  
 *  @param [in] short c1 First RNA
 *  @param [in] short c2 Second RNA
 *  @param [in] short c3 Third RNA
 *  @return Codon encoded as short usable by dict.c
 */
short getCodonNumFromRNANum(short c1, short c2, short c3) {
    if(c1<0 || c1>3 ||c1<0 || c1>3 ||c1<0 || c1>3) {
        fprintf(stderr,"dict.c: Wrong RNA Num Supplied! c1=%i c2=%i c3=%i\n",c1,c2,c3);
        return -1;
    }
    return (c1<<4) | (c2<<2) | c3;
}
