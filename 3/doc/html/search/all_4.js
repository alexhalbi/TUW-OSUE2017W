var searchData=
[
  ['getclientdata',['getClientData',['../mrna-server_8c.html#acc0f9895f0d50d1d7ef7705f3fcbded2',1,'mrna-server.c']]],
  ['getcodonfromnum',['getCodonFromNum',['../dict_8c.html#a85462d2c9e77999807ee9a62d807f456',1,'getCodonFromNum(short num, char *cod):&#160;dict.c'],['../dict_8h.html#aeb6ff76950928c142c41467994340ded',1,'getCodonFromNum(short, char *):&#160;dict.c']]],
  ['getcodonnumfromrnanum',['getCodonNumFromRNANum',['../dict_8c.html#a4d2bdaa4e1e13f1ca1560e6cac4ae7b5',1,'getCodonNumFromRNANum(short c1, short c2, short c3):&#160;dict.c'],['../dict_8h.html#a80e7f9cf0b90f823d0e00a59245e7553',1,'getCodonNumFromRNANum(short, short, short):&#160;dict.c']]],
  ['getnumfromcodon',['getNumFromCodon',['../dict_8c.html#a33e72713d8101ebf893dce174bcc1e58',1,'getNumFromCodon(char *cod):&#160;dict.c'],['../dict_8h.html#aee503ee3c426b4caf60d35b921753614',1,'getNumFromCodon(char *):&#160;dict.c']]],
  ['getnumfromprot',['getNumFromProt',['../dict_8c.html#ac2570e3d0fd763d26c2b346d90b61036',1,'getNumFromProt(char *c):&#160;dict.c'],['../dict_8h.html#a183d42e9aadea73cb75f39714f31057f',1,'getNumFromProt(char *):&#160;dict.c']]],
  ['getnumfromprotshort',['getNumFromProtShort',['../dict_8c.html#a6a0d48349f5a50f60ccb20df000160c8',1,'getNumFromProtShort(char c):&#160;dict.c'],['../dict_8h.html#a257759cd6be6c5237ceb47b7644fd7b5',1,'getNumFromProtShort(char):&#160;dict.c']]],
  ['getnumfromrna',['getNumFromRNA',['../dict_8c.html#a021c09b03ddebb78d04a1409b1355f23',1,'getNumFromRNA(char c):&#160;dict.c'],['../dict_8h.html#a3dcd90160dcdeedd6f6f2b06a7b56d56',1,'getNumFromRNA(char):&#160;dict.c']]],
  ['getprotfromnum',['getProtFromNum',['../dict_8c.html#a28daab94e0ac47c238d6d4e18bde43dc',1,'getProtFromNum(short num, char *prot):&#160;dict.c'],['../dict_8h.html#ac67137ebe73d5da84490cc8178f25249',1,'getProtFromNum(short, char *):&#160;dict.c']]],
  ['getprotfromrna',['getProtFromRNA',['../dict_8c.html#ae6533e025a4cd3b7a62390c81feabc56',1,'getProtFromRNA(short num):&#160;dict.c'],['../dict_8h.html#ae3acc4d9f5c525fa81b1d6ccaf0fe61c',1,'getProtFromRNA(short):&#160;dict.c']]],
  ['getprotshortfromnum',['getProtShortFromNum',['../dict_8c.html#abe86cd41edc1590e76a3aa283c653fd2',1,'getProtShortFromNum(short num):&#160;dict.c'],['../dict_8h.html#a03eec1bc364d135a424ff5affba8d3f1',1,'getProtShortFromNum(short):&#160;dict.c']]],
  ['getrnafromnum',['getRNAFromNum',['../dict_8c.html#a8be9e12c9415bfd865a73980ca7c0dac',1,'getRNAFromNum(short num):&#160;dict.c'],['../dict_8h.html#a95f558cf1d57b17198bed2ad54e532d8',1,'getRNAFromNum(short):&#160;dict.c']]],
  ['getrnafromprot',['getRNAFromProt',['../dict_8c.html#a4aca4ef4a475bbd84ddacf8d8c51ff95',1,'getRNAFromProt(short prot):&#160;dict.c'],['../dict_8h.html#a582864b6b0a7080c4b15324a3f68dc19',1,'getRNAFromProt(short):&#160;dict.c']]]
];
