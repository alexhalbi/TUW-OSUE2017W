var searchData=
[
  ['cleanup',['cleanup',['../mrna-client_8c.html#a5c6551bf9ea9cf51ae7f2076c4cbe8ef',1,'cleanup(void):&#160;mrna-client.c'],['../mrna-server_8c.html#a5c6551bf9ea9cf51ae7f2076c4cbe8ef',1,'cleanup(void):&#160;mrna-server.c']]],
  ['clientdata',['clientdata',['../mrna-server_8c.html#a0e80532582ecc5d13b6c99f8d47eba1c',1,'mrna-server.c']]],
  ['clientdatastruct',['clientdatastruct',['../structclientdatastruct.html',1,'']]],
  ['close_5fmsg',['CLOSE_MSG',['../mrna-client_8h.html#ab8f6dd31451b660308298d2cef10ac95',1,'mrna-client.h']]],
  ['colors_2eh',['colors.h',['../colors_8h.html',1,'']]],
  ['com_5fnext',['COM_NEXT',['../mrna-client_8h.html#aeceb4b06d526cecfef4f77b28f9ac43d',1,'mrna-client.h']]],
  ['com_5fquit',['COM_QUIT',['../mrna-client_8h.html#af9cd78f9935a6273db924a93947bc661',1,'mrna-client.h']]],
  ['com_5frest',['COM_REST',['../mrna-client_8h.html#ac1ab7e50e63199003f5226b0f2a039e6',1,'mrna-client.h']]],
  ['com_5fsubm',['COM_SUBM',['../mrna-client_8h.html#a65296d7b72443a47dcd33e57a40c92cf',1,'mrna-client.h']]],
  ['comm_2eh',['comm.h',['../comm_8h.html',1,'']]],
  ['commands',['COMMANDS',['../mrna-client_8h.html#ac90300e1e2bc5e5b092407b9c5267c9a',1,'mrna-client.h']]],
  ['cursor',['cursor',['../structclientdatastruct.html#a4da36833c786a362c06e64ba4fd52884',1,'clientdatastruct']]]
];
