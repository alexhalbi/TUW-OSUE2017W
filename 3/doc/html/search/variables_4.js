var searchData=
[
  ['rna',['RNA',['../dict_8c.html#a6ac569495494f5ea628d722c37e01b78',1,'dict.c']]],
  ['rna_5flength',['RNA_LENGTH',['../dict_8c.html#aa09529d77928a90bcf30b902a2e91af7',1,'RNA_LENGTH():&#160;dict.c'],['../dict_8h.html#aa09529d77928a90bcf30b902a2e91af7',1,'RNA_LENGTH():&#160;dict.h']]],
  ['rna_5fprot',['RNA_PROT',['../dict_8c.html#a6d8a2b1f064508ac6c92f026c98cdef7',1,'dict.c']]],
  ['rna_5fprot_5flength',['RNA_PROT_LENGTH',['../dict_8c.html#a554f87a5769e75b57bf5a9f1f9277a24',1,'RNA_PROT_LENGTH():&#160;dict.c'],['../dict_8h.html#a554f87a5769e75b57bf5a9f1f9277a24',1,'RNA_PROT_LENGTH():&#160;dict.h']]]
];
