/**
 * @file stegit.h
 * @author Alexander Halbarth <alexander.halbarth@student.tuwien.ac.at>
 * @date 01.11.2017
 *
 * @brief Main program Header File of stegit.
 * 
 * Header File for stegit.c
 **/

#define MAX_LENGTH 300 /**< Maximum Length of plain text input */
