/**
 * @file stegit.c
 * @author Alexander Halbarth <alexander.halbarth@student.tuwien.ac.at>
 * @date 01.11.2017
 *
 * @brief Main program module of stegit.
 * 
 * @details This Program has two modes: Hide and Find.
 * It hides a text in a bigger test and replaces all characters with words of the other text.
 * For informations of hide go to function hide()
 * For informations of find go to function find()
 * 
 * Usage: stegit -f|-h [-o <filename>]
 *
 * -f					find mode
 * -h					hide mode
 * [-o <filename>]		output filename
 **/

#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include "stegit.h"
#include "dict.h"

const char * argv0; /**< The program name */

/**
 *  @brief print usage function prototype
 */
static void prt_usage(void);

/**
 *  @brief hide function prototype
 */
static void hide(FILE*);

/**
 *  @brief find function prototype
 */
static void find(FILE*);

/**
 *  @brief random function prototype
 */
static double rnd(void);


/**
 * Program entry point.
 * @brief The program starts here. This function takes care about parameters, and if the
 * starts the acording subfunction find or hide if everything is fine. Else it terminates with EXIT_FAILURE and prints out the usage on usage failures
 * @details global variables: argv0
 * @param argc The argument counter.
 * @param argv The argument vector.
 * @return Returns EXIT_SUCCESS on Sucess and EXIT_FAILURE on Failure.
 */
int main(int argc, char **argv)
{
	argv0 = argv[0]; // Set Program Name
	int c;
	FILE * outputfile = NULL;
	int mode=0; // Mode of Program 0=unspecified, 1=find, 2=hide, -1=err
	while( (c=getopt(argc,argv, "o:fh")) != -1) {
		switch(c) {
			case 'f': // Find mode
				if(mode==0)
					mode=1;
				else
					mode=-1;
				break;
			case 'h': // Hide mode 
				if(mode==0)
					mode=2;
                else
                    mode=-1;
				break;
			case 'o': // output File specified
				if(optarg!=NULL && outputfile==NULL) {
					outputfile = fopen(optarg, "w");
					if(outputfile == NULL) {
						fprintf(stderr, "%s: fopen failed: %s\n",argv0, strerror(errno));
						exit(EXIT_FAILURE);
					}
				} else {
					fprintf(stderr,"%s: No or duplicate Output File specified\n",argv0);
					prt_usage();
					exit(EXIT_FAILURE);
				}
				break;
			default: // Error on wrong Arguments
				fprintf(stderr,"%s: Undefined Option\n",argv0);
                prt_usage();
                exit(EXIT_FAILURE);
				break;
		}
	}
	if((argc-optind) != 0) { //no more arguments supplied
		prt_usage();
        exit(EXIT_FAILURE);
	}
	if(outputfile == NULL) { // Default case for Output is stdout
        	outputfile = stdout;
	}
	switch(mode) {
		case 1:
			find(outputfile);
			break;
		case 2:
			hide(outputfile);
			break;
		default:
			fprintf(stderr,"%s: No or Duplicate Mode specified!\n",argv0);
	        prt_usage();
        	exit(EXIT_FAILURE);
			break;
	}
	if(fputs("\n",outputfile)==EOF) { // Add \n on the end of the output before closing the file.
		fprintf(stderr, "%s: fputs failed: %s\n",argv0, strerror(errno));
        exit(EXIT_FAILURE);
	}
	if(fclose(outputfile)!=0) { // Closing the file 
		fprintf(stderr, "%s: fclose failed: %s\n",argv0, strerror(errno));
        exit(EXIT_FAILURE);
	}
	return EXIT_SUCCESS;
}

/**
 *  @brief Encrypts Input with help of the Dictionary in dict.c
 *  
 *  @param [in] file The Output File
 *  
 *  @details In Hide Mode the program reads stdin until EOF or newline. After that the read message is hidden in a text and returned on stdout or in the specified output file.
 *  It should create random points all 5-15 Words.
 *  
 *  global variables: argv0
 */
static void hide(FILE *file) {
	// 1. Reading of the Input to encrypt from stdin (Until EOF or newline)
	char text [MAX_LENGTH];
	if(fgets(text, MAX_LENGTH, stdin)==NULL) {
		fprintf(stderr, "%s: fgets failed: %s\n",argv0, strerror(errno));
        exit(EXIT_FAILURE);
	}
	// 2. Encryption and output of the text to stdout or Output File
	int r = rnd()*10+4; // Random Number to make point
	for(int i = 0; strncmp(&text[i],"\0",1)!=0;i++) {
		if(fputs(getdictentry(text[i]),file)==EOF) {
			fprintf(stderr, "%s: fputs failed: %s\n",argv0, strerror(errno));
			exit(EXIT_FAILURE);
		}
		if(r==0) {
			fputs(". ",file);
			r = rnd() * 10 + 4;
		} else {
			fputc(' ',file);
			r--;
		}
	}
}

/**
 *  @brief Decrypts Input with help of the Dictionary in dict.c
 *  
 *  @param [in] file Outputfile
 *  
 *  @details In the find mode the program should read stdin until EOF. 
 *  On the end of the Textit should try to get the secret Message in the Text and returns it in stdout or the -o Output File.
 *  If an output file is generated it should be replaced or created. 
 *  global variables: argv0
 */
static void find(FILE *file) {
	// 1. Reading of the Input to decrypt from stdin (Until EOF or newline)
	char text [MAX_LENGTH * (LONGEST_WORD+2)]; // Biggest Input: Maximum Length of plaintext times biggest word plus 2
	if(fgets(text, MAX_LENGTH * (LONGEST_WORD+2), stdin)==NULL) {
		fprintf(stderr, "%s: fgets failed: %s\n",argv0, strerror(errno));
        exit(EXIT_FAILURE);
	}
	// 2. Decryption and output of the text to stdout or Output File
	char * str = strtok(text, " .\n\r");
	while(str!=NULL) {
		if(fputc(getrevdictentry(str),file)==EOF) {
			fprintf(stderr, "%s: fputc failed: %s\n",argv0, strerror(errno));
			exit(EXIT_FAILURE);
		} 
		str = strtok(NULL, " .\n\r");
	}
}

/**
 * @brief Creates random double from 0 to 1 with rand function.
 * @return Returns Random Double from 0 to 1
 */
static double rnd(void) {
	srand(time(NULL));
	return ((double) rand() / (double) RAND_MAX);
}

/**
 * @brief This function writes helpful usage information about the program to stderr.
 * @details global variables: argv0
 */
static void prt_usage(void) {
	fprintf(stderr, "%s -f|-h [-o <filename>]\n\n-f\tfind mode\n-h\thide mode\n[-o <filename>]\toutput filename\n", argv0);
}
