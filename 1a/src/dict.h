/**
 * @file dict.h
 * @author Alexander Halbarth <alexander.halbarth@student.tuwien.ac.at>
 * @date 01.11.2017
 *
 * @brief Dictionary header module of stegit.
 * 
 * This Header file contains the declarations for the Dictionary to de- and encrypt of the Input and all Methods to search the Dictionary Entries.
 **/

const int LONGEST_WORD; /**< Length of the longest word in the dictionary. Needed for calculation of the longest encrypted Text. */

/**
 *  Struct for storing Dictionary entries.
 */
struct dictentry {
	char single_char; /**< The character used to identify the word. */
	char* word; /**< The string used to identify the character. */
};


char* getdictentry(char);


char getrevdictentry(char*);

