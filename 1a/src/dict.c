/**
 * @file dict.c
 * @author Alexander Halbarth <alexander.halbarth@student.tuwien.ac.at>
 * @date 01.11.2017
 *
 * @brief Dictionary program module of stegit.
 * 
 * This Module contains the Dictionary to de- and encrypt of the Input and all Methods to search the Dictionary Entries.
 **/

#include <ctype.h>
#include <string.h>
#include "dict.h"

/**
 *  Dictionary definition Array of dictentry Struct.
 *  Statically filled
 */
static const struct dictentry DICT[] = {
{'a',"Alphabet"},
{'b',"Beta"},
{'c',"Chair"},
{'d',"Dumb"},
{'e',"Elephant"},
{'f',"Font"},
{'g',"Gustavo"},
{'h',"House"},
{'i',"Irma"},
{'j',"Jazz"},
{'k',"Klara"},
{'l',"Love"},
{'m',"Mountain"},
{'n',"North"},
{'o',"Omaha"},
{'p',"Pebble"},
{'q',"Que"},
{'r',"Rest"},
{'s',"Stair"},
{'t',"Trunk"},
{'u',"UFO"},
{'v',"Venezia"},
{'w',"Walter"},
{'x',"Xaver"},
{'y',"Ypsilon"},
{'z',"Zeppelin"},
{'.',"Pointer"},
{' ',"Nothing"}
};

/**
 *  Static Method Prototype
 */
static void strtolower(char*);

const int LONGEST_WORD = 8; /**< Length of the longest word in the dictionary. Needed for calculation of the longest encrypted Text. */

static const int LENGTH = sizeof(DICT)/sizeof(DICT[0]); /**< Size of Dictionary used for Itaration through it.*/

/**
 *  @brief Searches for Character in dictionary and returns fitting word to it.
 *  
 *  @param [in] c Character which shall be searched. Is changed to lower-case!
 *  @return Pointer to Word of the Dictionary fitting to the Character. If no entry is found Pointer Pointer to "\0" is returned (=empty String)
 *  
 *  @details Iterates with loop through Dictionary Array and searches for matching entry. If there are duplicates, it returns the entry with the lowest index. Search is not casesensitive.
 */
char* getdictentry(char c) {
	c = tolower(c);/** conversion to lower case */
	for(int i = 0; i<LENGTH;i++) {/** iteration through dictionary */
		if(DICT[i].single_char==c) { /** comparing of characters */
			return DICT[i].word; /** return word if matching */
		}
	}
	char *n = "\0";
	return n; /** no matches found */
}

/**
 *  @brief Searches backwards for word in dictionary and returns fitting character to it.
 *  
 *  @param [in] word Word which should be searched. Is changed to lower-case!
 *  @return Character wich fits to the word. If no entry is found 0 will be returned. (= String delimiter Character)
 *  
 *  @details Iterates with loop through Dictionary Array and searches for matching entry. If there are duplicates, it returns the entry with the lowest index. Search is not casesensitive.
 */
char getrevdictentry(char* word) {
        strtolower(word); /** Convertion input to lower case */
        for(int i = 0; i<LENGTH;i++) { /** iteration through dictionary */
			char entr[LONGEST_WORD]; /** placeholder for copy of word in the dictionary */
			strcpy(entr,DICT[i].word); /** Copy of word */
			strtolower(entr); /** conversion to lower case */
			if(strncmp(entr,word,LONGEST_WORD)==0) { /** comparing of string */
					return DICT[i].single_char; /** return character if matching */
			}
        }
        return 0; /** no matches found */
}

/**
 *  @brief Converts String to lower case
 *  
 *  @param [in] str String which will be changed to Lower case
 *  @return String in Lower case in the supplied pointer
 *  
 *  @details Uses tolower to convert each character in the string to lower case. String needs to be delimited by \0
 */
static void strtolower(char * str) {
	for(int i = 0; str[i]!=0; i++) {
                str[i]=tolower(str[i]);
        }
}
